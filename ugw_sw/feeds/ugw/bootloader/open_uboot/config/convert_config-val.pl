#!/usr/bin/env perl
# 
# Copyright (C) 2006 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#SE for more information.
#

use warnings;
use strict;

use FindBin;
use lib $FindBin::Bin;

my $override_fname = "$FindBin::Bin/.config-owrt-to-uboot-override";
my %override;

my $PREFIX = "CONFIG_";
my $top_config_fname = "$ENV{TOPDIR}/.config";
my %top_config;

my %uboot_cfg;

sub parse_override($)
{
	my $cfg = shift;
	my $fd;
	my %result;

	open $fd, "<", $cfg or
	die "Unable to open config file $cfg";
	while (<$fd>) {
		chomp;
		my ($owrt, $uboot) = split(/\s+/, $_, 2); 
		$result{$owrt} = $uboot;
	}
	close $fd;

	return \%result;
}

sub set_config($$$$) {
	my $config = shift;
	my $idx = shift;
	my $newval = shift;
	my $mod_plus = shift;

	if (!defined($config->{$idx}) or !$mod_plus or
	    $config->{$idx} eq '#undef' or $newval eq 'y') {
		$config->{$idx} = $newval;
	}
}
	
sub load_config($) {
	my $file = shift;
	my $mod_plus = shift;
	my %config;

	open FILE, "$file" or die "can't open file '$file'";
	while (<FILE>) {
		chomp;
		/^($PREFIX.+?)=(.+)/ and do {
			set_config(\%config, $1, $2, $mod_plus);
			next;
		};
		/^# ($PREFIX.+?) is not set/ and do {
			set_config(\%config, $1, "#undef", $mod_plus);
			next;
		};
		/^#/ and next;
		/^(.+)$/ and warn "WARNING: can't parse line: $1\n";
	}
	return \%config;
}

sub print_cfgline($$) {
	my $name = shift;
	my $val = shift;
	if ($val eq '#undef' or $val eq 'n') {
		print "# $name is not set\n";
	} else {
		print "$name=$val\n";
	}
}

sub dump_config($) {
	my $cfg = shift;
	die "argument error in dump_config" unless ($cfg);
	my %config = %$cfg;
	foreach my $config (sort keys %config) {
		print_cfgline($config, $config{$config});
	}
}

sub filter_override($$) {
	my $cfg1 = shift;
	my $cfg2 = shift;
	my %top_config_p = %{$cfg1};
	my %override_p = %{$cfg2};
	my %filter;

	foreach my $or (sort keys %override_p) {
		if (defined($top_config_p{$or})) {
			#print_cfgline($override_p{$or}, $top_config_p{$or});
			$filter{$override_p{$or}} = $top_config_p{$or};
		} else {
			# not exist in toplevel config
			# what to do ?
			# uncomment to set it ot no anyway 
			# $filter{$override_p{$or}} = "#undef";
		}
	}

	return \%filter;
}

sub dump_override()
{
	my $olist;
	my $config;
	my $filter;

	# read owrt to uboot override file
	$olist = parse_override($override_fname);
	%override = %$olist;

	# read owrt toplevel .config and its value
	$config = load_config($top_config_fname);
	%top_config = %$config;

	# uboot config override
	# this is simple i get owrt .config value and put it to corresponding
	# uboot config key value based on convertion table in $override_fname
	$filter =  filter_override(\%top_config, \%override);
	%uboot_cfg = %$filter;

	# dump to console
	dump_config(\%uboot_cfg);
}

if (defined $ARGV[0]) {
	$top_config_fname = $ARGV[0];
}
if (defined $ARGV[1]) {
	$override_fname = $ARGV[1];
}

dump_override();
