ARCH:=mips
SUBTARGET:=falcon
BOARDNAME:=Falcon
FEATURES:=squashfs ramdisk
CPU_TYPE:=24kc
DEVICE_TYPE:=pon

KERNEL_PATCHVER:=4.9

DEFAULT_PACKAGES+= kmod-ifxos gpon-base-files kmod-leds-gpio kmod-ledtrig-heartbeat \
	kmod-gpon-optic-drv gpon-optic-drv kmod-gpon-onu-drv gpon-onu-drv \
	gpon-pe-firmware gpon-omci-api gpon-omci-onu gpon-luci gpon-dti-agent

define Target/Description
	Lantiq Falcon
endef
