ARCH:=mipsel
SUBTARGET:=svip_le
BOARDNAME:=SVIP Little Endian
FEATURES:=squashfs nand ubifs ramdisk
CPU_TYPE:=24kc
DEVICE_TYPE:=other

KERNEL_PATCHVER:=4.9

DEFAULT_PACKAGES+= uboot-svip

define Target/Description
	Lantiq SVIP Little Endian
endef
