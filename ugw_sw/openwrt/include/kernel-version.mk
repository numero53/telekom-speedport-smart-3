# Use the default kernel version if the Makefile doesn't override it

LINUX_RELEASE?=1

LINUX_VERSION-3.18 = .43
LINUX_VERSION-4.4 = .92
LINUX_VERSION-4.9 =
#LINUX_VERSION-4.9 = .13

LINUX_KERNEL_HASH-3.18.43 = 1236e8123a6ce537d5029232560966feed054ae31776fe8481dd7d18cdd5492c
LINUX_KERNEL_HASH-4.4.92 = 53f8cd8b024444df0f242f8e6ab5147b0b009d7a30e8b2ed3854e8d17937460d
LINUX_KERNEL_HASH-4.9 = 029098dcffab74875e086ae970e3828456838da6e0ba22ce3f64ef764f3d7f1a
LINUX_KERNEL_HASH-4.9.13 = 36464aec4fb32ed192252645819abb55ceb21eef17b3f7210b4a673e7486a268

ifdef KERNEL_PATCHVER
  LINUX_VERSION:=$(KERNEL_PATCHVER)$(strip $(LINUX_VERSION-$(KERNEL_PATCHVER)))
endif

split_version=$(subst ., ,$(1))
merge_version=$(subst $(space),.,$(1))
KERNEL_BASE=$(firstword $(subst -, ,$(LINUX_VERSION)))
KERNEL=$(call merge_version,$(wordlist 1,2,$(call split_version,$(KERNEL_BASE))))
KERNEL_PATCHVER ?= $(KERNEL)

# disable the md5sum check for unknown kernel versions
LINUX_KERNEL_HASH:=$(LINUX_KERNEL_HASH-$(strip $(LINUX_VERSION)))
LINUX_KERNEL_HASH?=x
