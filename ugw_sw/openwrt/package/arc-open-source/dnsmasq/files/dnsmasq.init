#!/bin/sh /etc/rc.common
# Copyright (C) 2007 OpenWrt.org

START=60

[ -f /tmp/br_modem_enabled ] && { 
	echo "### dnsmasq.init is disabled in bridge modem mode .." > /dev/console 
	exit 0
}

[ -f /tmp/wifi_access_point_enabled ] && { 
	echo "### dnsmasq.init is disabled in WIFI AP mode .." > /dev/console
	exit 0
}

USE_PROCD=1
PROG=/usr/sbin/dnsmasq

DNS_SERVERS=""
DOMAIN=""

ADD_LOCAL_DOMAIN=1
ADD_LOCAL_HOSTNAME=1

CONFIGFILE="/var/etc/dnsmasq.conf"
HOSTFILE="/tmp/hosts/dhcp"

#DNSMASQ_FILE=/tmp/dnsmasq.conf
#DHCPD_FILE=/tmp/dhcpd.conf
#DHCPD_LEASES=/tmp/dhcpd.leases
DNSSVR_FILE=/tmp/resolv.conf
STATIC_DHCP_FILE=/tmp/dhcp.staticip

LANIF="br-lan"
LOOPIF="lo"

xappend() {
	local value="$1"

	echo "${value#--}" >> $CONFIGFILE
}

dhcp_calc() {
	local ip="$1"
	local res=0

	while [ -n "$ip" ]; do
		part="${ip%%.*}"
		res="$(($res * 256))"
		res="$(($res + $part))"
		[ "${ip%.*}" != "$ip" ] && ip="${ip#*.}" || ip=
	done
	echo "$res"
}

dhcp_check() {
	local ifname="$1"
	local stamp="/var/run/dnsmasq.$ifname.dhcp"
	local rv=0

	[ -s "$stamp" ] && return $(cat "$stamp")

	udhcpc -n -q -s /bin/true -t 1 -i "$ifname" >&- && rv=1 || rv=0

	[ $rv -eq 1 ] && \
		logger -t dnsmasq \
			"found already running DHCP-server on interface '$ifname'" \
			"refusing to start, use 'option force 1' to override"

	echo $rv > "$stamp"
	return $rv
}

log_once() {
	pidof dnsmasq >/dev/null || \
		logger -t dnsmasq "$@"
}

append_bool() {
	local section="$1"
	local option="$2"
	local value="$3"
	local _loctmp
	config_get_bool _loctmp "$section" "$option" 0
	[ "$_loctmp" -gt 0 ] && xappend "$value"
}

append_parm() {
	local section="$1"
	local option="$2"
	local switch="$3"
	local _loctmp
	config_get _loctmp "$section" "$option"
	[ -z "$_loctmp" ] && return 0
	xappend "$switch=$_loctmp"
}

append_server() {
	xappend "--server=$1"
}

append_address() {
	xappend "--address=$1"
}

append_ipset() {
	xappend "--ipset=$1"
}

append_interface() {
	local ifname=$(uci_get_state network "$1" ifname "$1")
	xappend "--interface=$ifname"  #"-i $ifname"
}

append_notinterface() {
	local ifname=$(uci_get_state network "$1" ifname "$1")
	xappend "--except-interface=$ifname"  #"-I $ifname"
}

append_addnhosts() {
	xappend "--addn-hosts=$1"  #"-H $1"
}

append_bogusnxdomain() {
	xappend "--bogus-nxdomain=$1"  #"-B $1"
}

# --dt-dhcpd-domain : '.<ARC_LAN_0_DomainName>'
# --domain : '<ARC_LAN_0_DomainName>'
# notic: W724 FW ver does not contain '.'. Dnsmasq only take domain name after '.' character.
append_dtdomain() {
	local switch=$1
	local domain="`mng_cli get ARC_LAN_0_DomainName`" 
	if [ "${switch}" = "--domain" ]; then
		xappend "$switch=${domain}"
	else	# --dt-dhcpd-domain
		xappend "$switch=.${domain}"
	fi
}

dnsmasq() {
	local cfg="$1"
	append_bool "$cfg" authoritative "--dhcp-authoritative"  #"-K"
	append_bool "$cfg" nodaemon "--no-daemon"  #"-d"
	append_bool "$cfg" domainneeded "--domain-needed"  #"-D"
	append_bool "$cfg" filterwin2k "--filterwin2k"  #"-f"
	append_bool "$cfg" nohosts "--no-hosts"  #"-h"
	append_bool "$cfg" nonegcache "--no-negcache"  #"-N"
#	append_bool "$cfg" strictorder "--strict-order"  #"-o"
	append_bool "$cfg" logqueries "--log-queries"  #"-q"
	append_bool "$cfg" noresolv "--no-resolv"  #"-R"
	append_bool "$cfg" localise_queries "--localise-queries"  #"-y"
	append_bool "$cfg" readethers "--read-ethers"  #"-Z"
	append_bool "$cfg" dbus "--enable-dbus"  #"-1"
	append_bool "$cfg" boguspriv "--bogus-priv"  #"-b"
	append_bool "$cfg" expandhosts "--expand-hosts"  #"-E"
#	append_bool "$cfg" enable_tftp "--enable-tftp"
#	append_bool "$cfg" nonwildcard "--bind-interfaces"  #"-z"

#	append_parm "$cfg" dhcpscript "--dhcp-script"  #"-6"
#	append_parm "$cfg" cachesize "--cache-size"  #"-c"
	append_parm "$cfg" dnsforwardmax "--dns-forward-max"  #"-0"
	append_parm "$cfg" port "--port"  #"-p"
	append_parm "$cfg" ednspacket_max "--edns-packet-max"  #"-P"
	append_parm "$cfg" dhcpleasemax "--dhcp-lease-max"  #"-X"
	append_parm "$cfg" "queryport" "--query-port"  #"-Q"
	#append_parm "$cfg" "domain" "--domain"  #"-s"
	#append_parm "$cfg" "local" "--server"  #"-S"
	config_list_foreach "$cfg" "server" append_server
	config_list_foreach "$cfg" "address" append_address
	config_list_foreach "$cfg" "ipset" append_ipset
#	config_list_foreach "$cfg" "interface" append_interface
	config_list_foreach "$cfg" "notinterface" append_notinterface
	config_list_foreach "$cfg" "addnhosts" append_addnhosts
	config_list_foreach "$cfg" "bogusnxdomain" append_bogusnxdomain
	append_parm "$cfg" "leasefile" "--dhcp-leasefile"  #"-l"
#	append_parm "$cfg" "resolvfile" "--resolv-file"  #"-r"
#	append_parm "$cfg" "tftp_root" "--tftp-root"
#	append_parm "$cfg" "dhcp_boot" "--dhcp-boot"
	### ctc ###
#	append_parm "$cfg" "dhcpscript" "-6"
	###########
	### sean ###
	xappend "--bind-interfaces"	#"-z"
	xappend "--interface=br-lan"  	#"-i $ifname"
	xappend "--resolv-file=/tmp/resolv.conf.auto" 			#"-r"
	xappend "--no-poll" 			#"-n"
	xappend "--min-port=11000"		#"--min-port"
	xappend "--strict-order" 		#"-o"
	xappend "--cache-size=5000" 	#"-c"
	#append_dtdomain "--domain"
	append_dtdomain "--dt-dhcpd-domain"
	###########

	config_get DOMAIN "$cfg" domain

	config_get_bool ADD_LOCAL_DOMAIN "$cfg" add_local_domain 1
	config_get_bool ADD_LOCAL_HOSTNAME "$cfg" add_local_hostname 1

	config_get_bool readethers "$cfg" readethers
	[ "$readethers" = "1" -a \! -e $STATIC_DHCP_FILE ] && touch $STATIC_DHCP_FILE

#	config_get leasefile $cfg leasefile
	########## ctc ############
#	[ -n "$leasefile" -a \! -e "$leasefile" ] && touch "$leasefile"
#	[ -n "$leasefile" -a \! -e "$leasefile" ] && ( echo -n > "$leasefile" )
	###########################
	config_get_bool cachelocal "$cfg" cachelocal 1

	config_get hostsfile "$cfg" dhcphostsfile
	[ -e "$hostsfile" ] && xappend "--dhcp-hostsfile=$hostsfile"

	mkdir -p /tmp/hosts /tmp/dnsmasq.d
	xappend "--addn-hosts=/tmp/hosts"
	xappend "--conf-dir=/tmp/dnsmasq.d"

	local rebind
	config_get_bool rebind "$cfg" rebind_protection 0
	[ $rebind -gt 0 ] && {
		log_once \
			"DNS rebinding protection is active," \
			"will discard upstream RFC1918 responses!"
		xappend "--stop-dns-rebind"

		local rebind_localhost
		config_get_bool rebind_localhost "$cfg" rebind_localhost 0
		[ $rebind_localhost -gt 0 ] && {
			log_once "Allowing 127.0.0.0/8 responses"
			xappend "--rebind-localhost-ok"
		}

		append_rebind_domain() {
			log_once "Allowing RFC1918 responses for domain $1"
			xappend "--rebind-domain-ok=$1"
		}

#		config_list_foreach "$cfg" rebind_domain append_rebind_domain
		domainname=`mng_cli get ARC_LAN_0_DomainName`
		if [ -n "$domainname" ] ; then
			append_rebind_domain "$domainname"
		fi
	}

	dhcp_option_add "$cfg" "" 0

	#xappend "--dhcp-broadcast=tag:needs-broadcast"

	echo >> $CONFIGFILE
}

dhcp_subscrid_add() {
	local cfg="$1"

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] || return 0

	config_get subscriberid "$cfg" subscriberid
	[ -n "$subscriberid" ] || return 0

	xappend "--dhcp-subscrid=$networkid,$subscriberid"

	dhcp_option_add "$cfg" "$networkid"
}

dhcp_remoteid_add() {
	local cfg="$1"

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] || return 0

	config_get remoteid "$cfg" remoteid
	[ -n "$remoteid" ] || return 0

	xappend "--dhcp-remoteid=$networkid,$remoteid"

	dhcp_option_add "$cfg" "$networkid"
}

dhcp_circuitid_add() {
	local cfg="$1"

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] || return 0

	config_get circuitid "$cfg" circuitid
	[ -n "$circuitid" ] || return 0

	xappend "--dhcp-circuitid=$networkid,$circuitid"

	dhcp_option_add "$cfg" "$networkid"
}

dhcp_userclass_add() {
	local cfg="$1"

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] || return 0

	config_get userclass "$cfg" userclass
	[ -n "$userclass" ] || return 0

	xappend "--dhcp-userclass=$networkid,$userclass"

	dhcp_option_add "$cfg" "$networkid"
}

dhcp_vendorclass_add() {
	local cfg="$1"

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] || return 0

	config_get vendorclass "$cfg" vendorclass
	[ -n "$vendorclass" ] || return 0

	xappend "--dhcp-vendorclass=$networkid,$vendorclass"

	dhcp_option_add "$cfg" "$networkid"
}

dhcp_host_add() {
	local cfg="$1"

	config_get name "$cfg" name

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] && dhcp_option_add "$cfg" "$networkid"

	config_get name "$cfg" name
	config_get ip "$cfg" ip
	[ -n "$ip" -o -n "$name" ] || return 0

	config_get_bool dns "$cfg" dns 0
	[ "$dns" = "1" -a -n "$ip" -a -n "$name" ] && {
		echo "$ip $name${DOMAIN:+.$DOMAIN}" >> $HOSTFILE
	}

	config_get mac "$cfg" mac
	if [ -n "$mac" ]; then
		# --dhcp-host=00:20:e0:3b:13:af,192.168.0.199,lap
		macs=""
		for m in $mac; do append macs "$m" ","; done
	else
		# --dhcp-host=lap,192.168.0.199
		[ -n "$name" ] || return 0
		macs="$name"
		name=""
	fi

	config_get tag "$cfg" tag

	config_get_bool broadcast "$cfg" broadcast 0
	[ "$broadcast" = "0" ] && broadcast=

	xappend "--dhcp-host=$macs${networkid:+,net:$networkid}${broadcast:+,set:needs-broadcast}${tag:+,set:$tag}${ip:+,$ip}${name:+,$name}"
}

dhcp_tag_add() {
	local cfg="$1"

	tag="$cfg"

	[ -n "$tag" ] || return 0

	config_get_bool force "$cfg" force 0
	[ "$force" = "0" ] && force=

	config_get option "$cfg" dhcp_option
	for o in $option; do
		xappend "--dhcp-option${force:+-force}=tag:$tag,$o"
	done
}

dhcp_mac_add() {
	local cfg="$1"

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] || return 0

	config_get mac "$cfg" mac
	[ -n "$mac" ] || return 0

	xappend "--dhcp-mac=$networkid,$mac"

	dhcp_option_add "$cfg" "$networkid"
}

dhcp_boot_add() {
	local cfg="$1"

	config_get networkid "$cfg" networkid

	config_get filename "$cfg" filename
	[ -n "$filename" ] || return 0

	config_get servername "$cfg" servername
	[ -n "$servername" ] || return 0

	config_get serveraddress "$cfg" serveraddress
	[ -n "$serveraddress" ] || return 0

	xappend "--dhcp-boot=${networkid:+net:$networkid,}$filename,$servername,$serveraddress"

	dhcp_option_add "$cfg" "$networkid"
}


dhcp_add() {
	# workaround
	# Some busybox version can not fully calculate the integer value from ip address to integer.
	# So, we set the first part to 1 to workaround this issue.
	# Ex: 192.168.2.2 => 1.168.2.2
	local start_A
	local start_B
	local start_C
	local start_D
	local subnet_A
	local subnet_B
	local subnet_C
	local subnet_D
	local subnet_save
	local START_A
	local START_B
	local START_C
	local START_D
	local END_A
	local END_B
	local END_C
	local END_D

	local cfg="$1"
	config_get net "$cfg" interface
	[ -n "$net" ] || return 0

	config_get dhcpv4 "$cfg" dhcpv4
	[ "$dhcpv4" != "disabled" ] || return 0

	config_get networkid "$cfg" networkid
	[ -n "$networkid" ] || networkid="$net"

	network_get_subnet subnet "$net" || return 0
	network_get_device ifname "$net" || return 0
	network_get_protocol proto "$net" || return 0

	[ "$cachelocal" = "0" ] && network_get_dnsserver dnsserver "$net" && {
		DNS_SERVERS="$DNS_SERVERS $dnsserver"
	}

	append_bool "$cfg" ignore "--no-dhcp-interface=$ifname" && return 0

	# Do not support non-static interfaces for now
	[ static = "$proto" ] || return 0

	# Override interface netmask with dhcp config if applicable
	config_get netmask "$cfg" netmask "${subnet##*/}"

	#check for an already active dhcp server on the interface, unless 'force' is set
	config_get_bool force "$cfg" force 0
	[ $force -gt 0 ] || dhcp_check "$ifname" || return 0

	config_get start "$cfg" start
	config_get limit "$cfg" limit
	config_get leasetime "$cfg" leasetime
	config_get options "$cfg" options
	config_get_bool dynamicdhcp "$cfg" dynamicdhcp 1

	leasetime="${leasetime:-12h}"

	start_A=$(echo $start | cut -d '.' -f1)
	start_B=$(echo $start | cut -d '.' -f2)
	start_C=$(echo $start | cut -d '.' -f3)
	start_D=$(echo $start | cut -d '.' -f4)
	start="1.$start_B.$start_C.$start_D"
	echo "start_A:$start_A,start_B:$start_B,start_C:$start_C,start_D:$start_D,start:$start" > /dev/console
	start="$(dhcp_calc "${start:-100}")"

	echo "subnet:$subnet,real subnet:${subnet%%/*}" > /dev/console
	subnet_save=${subnet%%/*}
	subnet_A=$(echo $subnet_save | cut -d '.' -f1)
	subnet_B=$(echo $subnet_save | cut -d '.' -f2)
	subnet_C=$(echo $subnet_save | cut -d '.' -f3)
	subnet_D=$(echo $subnet_save | cut -d '.' -f4)
	subnet="1.$subnet_B.$subnet_C.$subnet_D"

	limit="${limit:-150}"
	[ "$limit" -gt 0 ] && limit=$((limit-1))
	#eval "$(ipcalc.sh "${subnet%%/*}" $netmask $start $limit)"
	eval "$(ipcalc.sh "$subnet" $netmask $start $limit)"
	if [ "$dynamicdhcp" = "0" ]; then END="static"; fi

	START_A=$(echo $START | cut -d '.' -f1)
	START_B=$(echo $START | cut -d '.' -f2)
	START_C=$(echo $START | cut -d '.' -f3)
	START_D=$(echo $START | cut -d '.' -f4)
	START="$start_A.$START_B.$START_C.$START_D"
	echo "START_A:$START_A,START_B:$START_B,START_C:$START_C,START_D:$START_D,START:$START" > /dev/console

	END_A=$(echo $END | cut -d '.' -f1)
	END_B=$(echo $END | cut -d '.' -f2)
	END_C=$(echo $END | cut -d '.' -f3)
	END_D=$(echo $END | cut -d '.' -f4)
	END="$start_A.$END_B.$END_C.$END_D"
	echo "END_A:$END_A,END_B:$END_B,END_C:$END_C,END_D:$END_D,END:$END" > /dev/console
	echo "NETMASK:$NETMASK" > /dev/console

	xappend "--dhcp-range=$networkid,$START,$END,$NETMASK,$leasetime${options:+ $options}"

	dhcp_option_add "$cfg" "$networkid"
}

dhcp_option_add() {
	local cfg="$1"
	local networkid="$2"

	config_get dhcp_option "$cfg" dhcp_option
	for o in $dhcp_option; do
		xappend "-O $networkid","$o"
	done

}

dhcp_domain_add() {
	local cfg="$1"
	local ip name names

	config_get names "$cfg" name
	[ -n "$names" ] || return 0

	config_get ip "$cfg" ip
	[ -n "$ip" ] || return 0

	local oIFS="$IFS"; IFS="."; set -- $ip; IFS="$oIFS"
	local raddr="${4:+$4.$3.$2.$1.in-addr.arpa}"

	for name in $names; do
		local fqdn="$name"

		[ "${fqdn%.*}" == "$fqdn" ] && \
			fqdn="$fqdn${DOMAIN:+.$DOMAIN}"

		xappend "-A /$fqdn/$ip"

		[ -n "$raddr" ] && {
			xappend "--ptr-record=$raddr,$fqdn"
			raddr=""
		}
	done

	echo "$ip $fqdn" >> $HOSTFILE
}

dhcp_srv_add() {
	local cfg="$1"

	config_get srv "$cfg" srv
	[ -n "$srv" ] || return 0

	config_get target "$cfg" target
	[ -n "$target" ] || return 0

	config_get port "$cfg" port
	[ -n "$port" ] || return 0

	local service="$srv,$target"
	[ -n "$port" ] && service="$service,$port"

	xappend "-W $service"
}

dhcp_mx_add() {
	local cfg="$1"
	local domain relay pref

	config_get domain "$cfg" domain
	[ -n "$domain" ] || return 0

	config_get relay "$cfg" relay
	[ -n "$relay" ] || return 0

	config_get pref "$cfg" pref 0

	local service="$domain,$relay,$pref"

	xappend "--mx-host=$service"
}

dhcp_cname_add() {
	local cfg="$1"
	local cname target

	config_get cname "$cfg" cname
	[ -n "$cname" ] || return 0

	config_get target "$cfg" target
	[ -n "$target" ] || return 0

	xappend "--cname=${cname},${target}"
}

dhcp_hostrecord_add() {
	local cfg="$1"
	local names addresses record val

	config_get names "$cfg" name "$2"
	if [ -z "$names" ]; then
		return 0
	fi

	config_get addresses "$cfg" ip "$3"
	if [ -z "$addresses" ]; then
		return 0
	fi

	for val in $names $addresses; do
		record="${record:+$record,}$val"
	done

	xappend "--host-record=$record"
}

service_triggers()
{
	procd_add_reload_trigger "dhcp"
}

boot() {
	# Will be launched through hotplug
	return 0
}

start_service() {
	include /lib/functions

	config_load dhcp

	procd_open_instance
#	procd_set_param command $PROG -C $CONFIGFILE -H $HOSTFILE --dhcp-alternate-port=667,668 -k -u root
#	procd_set_param command $PROG -C $CONFIGFILE -H $HOSTFILE -k -u root
	dt_dhcpd_domain="`mng_cli get ARC_LAN_0_DomainName`"
	procd_set_param command $PROG --user=root -n -i br-lan -z -b -o --min-port 11000 -r /tmp/resolv.conf.auto -H /tmp/hosts/dhcp -c 5000 --dt-dhcpd-domain .$dt_dhcpd_domain
	procd_set_param file $CONFIGFILE
	procd_set_param respawn
	procd_close_instance

	# before we can call xappend
	mkdir -p $(dirname $CONFIGFILE)
	mkdir -p $(dirname $HOSTFILE)

	echo "# auto-generated config file from /etc/config/dhcp" > $CONFIGFILE
	echo "# auto-generated config file from /etc/config/dhcp" > $HOSTFILE

#	args="-cf $DHCPD_FILE -lf $DHCPD_LEASES $LANIF"
	config_foreach dnsmasq dnsmasq
#	config_foreach dhcp_host_add host
#	echo >> $CONFIGFILE
#	config_foreach dhcp_boot_add boot
#	config_foreach dhcp_mac_add mac
#	config_foreach dhcp_tag_add tag
#	config_foreach dhcp_vendorclass_add vendorclass
#	config_foreach dhcp_userclass_add userclass
#	config_foreach dhcp_circuitid_add circuitid
#	config_foreach dhcp_remoteid_add remoteid
#	config_foreach dhcp_subscrid_add subscrid
#	config_foreach dhcp_domain_add domain
#	config_foreach dhcp_hostrecord_add hostrecord

	# add own hostname
#	local lanaddr
#	[ $ADD_LOCAL_HOSTNAME -eq 1 ] && network_get_ipaddr lanaddr "lan" && {
#		local hostname="$(uci_get system @system[0] hostname OpenWrt)"
#		dhcp_domain_add "" "$hostname" "$lanaddr"
#	}

#	echo >> $CONFIGFILE
#	config_foreach dhcp_srv_add srvhost
#	config_foreach dhcp_mx_add mxhost
#	echo >> $CONFIGFILE

#	config_get odhcpd_is_active odhcpd maindhcp
#	if [ "$odhcpd_is_active" != "1" ]; then
#		config_foreach dhcp_add dhcp
#	fi

#	echo >> $CONFIGFILE
#	config_foreach dhcp_cname_add cname
#	echo >> $CONFIGFILE

	rm -f $DNSSVR_FILE    #/tmp/resolv.conf
	[ $ADD_LOCAL_DOMAIN -eq 1 ] && [ -n "$DOMAIN" ] && {
		echo "search $DOMAIN" >> $DNSSVR_FILE    #/tmp/resolv.conf
	}
	DNS_SERVERS="$DNS_SERVERS 127.0.0.1"
	for DNS_SERVER in $DNS_SERVERS ; do
		echo "nameserver $DNS_SERVER" >> $DNSSVR_FILE    #/tmp/resolv.conf
	done

	rm -rf $HOSTFILE
	lanip4addr=`mng_cli get ARC_LAN_0_IP4_Addr`
	echo "$lanip4addr speedport.ip" >> $HOSTFILE
	echo "fe80::1 speedport.ip" >> $HOSTFILE

# Mask by WindChen, don't sopt DHCPD anymore... 2012/06/14
#	/usr/sbin/dhcpd -cf $DHCPD_FILE -lf $DHCPD_LEASES $LANIF &
#

# Sean disables to use procd(PROG)
#	echo start dnsmasq!!! >/dev/console
#	/usr/sbin/dnsmasq --user=root -n -i $LANIF -I $LOOPIF -z -b --min-port 11000 -r /tmp/resolv.conf.auto -H $HOSTFILE
}

stop_process() {
#	killall -q -KILL dnsmasq

	local PID
	local PIDS

	PIDS=`ps | grep -w "/usr/sbin/dnsmasq" | awk '{ print $1 }'`
	for PID in $PIDS ; do
		if [ $PID -ne $$ ] && [ $PID -ne $PPID ] ; then
			kill ${PID} 2>&-
		fi
	done

#	--- Mask by WindChen, don't need dhcpd here --- 2012/06/14
#	PIDS=`ps | grep -w "dhcpd" | awk '{ print $1 }'`
#	for PID in $PIDS ; do
#		if [ $PID -ne $$ ] && [ $PID -ne $PPID ] ; then
#echo "++++++++++ dnsmasq.ini: [ $PID -ne $$ ] && [ $PID -ne $PPID ]  ++++++++++++++++++" > /dev/console
#			kill ${PID} 2>&-
#		fi
#	done

	return 0
}

reload_service() {
	rc_procd start_service "$@"
	return 0
}

stop_service() {
	[ -f $DNSSVR_FILE ] && {
		rm -f $DNSSVR_FILE
		ln -s ${DNSSVR_FILE}.auto $DNSSVR_FILE
	}
#Sean disables because procd would handle this.
#	killall -q -KILL dnsmasq

# Mask by WindChen, Don't need it anymore...2012/06/14
#echo "++++++++++ dnsmasq.ini: dhcpd killed  ++++++++++++++++++" > /dev/console
#	killall -q -KILL dhcpd
#
	return 0
}
