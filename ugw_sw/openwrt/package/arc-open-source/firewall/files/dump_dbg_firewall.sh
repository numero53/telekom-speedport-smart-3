#!/bin/sh


echo "--------------->Start iptables-save<-------------------"
iptables-save ${OUTDIR}
echo "--------------->End   iptables-save<-------------------"

echo "--------------->Start config/network<-------------------"
cat /etc/config/network
echo "--------------->End   config/network<-------------------"

echo "--------------->Start config/firewall<-------------------"
cat /etc/config/firewall
echo "--------------->End   config/firewall<-------------------"
