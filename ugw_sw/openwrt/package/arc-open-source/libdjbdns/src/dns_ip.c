#include "stralloc.h"
#include "uint16.h"
#include "byte.h"
#include "dns.h"

#if 1
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <stdio.h>

#define FONAP_RUN_DIR   "/tmp/fonap"
#define FONAP_LOG_SIZE  100000
#define FONAP_LOG_FLAG  FONAP_RUN_DIR "/fonap_debug_flag"
#define FONAP_FONSMCD_DEBUG_LOG_FILE    FONAP_RUN_DIR "/fonsmcd_debug.log"

#define FONAP_DEBUG(fmt, arg...) do {\
  FILE *fp; \
  struct stat stat_buf; \
  if ((stat(FONAP_LOG_FLAG, &stat_buf) == 0) && S_ISREG(stat_buf.st_mode)) { \
    if ((stat(FONAP_FONSMCD_DEBUG_LOG_FILE, &stat_buf) == 0) && (stat_buf.st_size < FONAP_LOG_SIZE)) { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "a"); \
    } else { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "w"); \
    } \
    if (fp) { \
      FILE *uptime_fp; \
      unsigned long up_s = 0; \
      unsigned long up_ns = 0; \
      unsigned long id_s = 0; \
      unsigned long id_ns = 0; \
      uptime_fp=fopen("/proc/uptime", "r"); \
      if (uptime_fp) { \
        fscanf(uptime_fp, "%lu.%02lu %lu.%02lu", &up_s, &up_ns, &id_s, &id_ns); \
        fclose(uptime_fp); \
      } \
      fprintf(fp, "[uptime:%lu.%02lu]: pid=[%d] ", up_s, up_ns, getpid()); \
      fprintf(fp, fmt, ## arg); \
      fprintf(fp, "\n"); \
      fclose(fp); \
    } \
  } \
} while (0)
#else
#define FONAP_DEBUG(fmt, arg...)
#endif


int dns_ip4_packet(stralloc *out,const char *buf,unsigned int len)
{
  unsigned int pos;
  char header[12];
  uint16 numanswers;
  uint16 datalen;
  int count = 0;

  if (!stralloc_copys(out,"")) return -1;

  pos = dns_packet_copy(buf,len,0,header,12); if (!pos) return -1;
  uint16_unpack_big(header + 6,&numanswers);
  pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
  pos += 4;

  while (numanswers--) {
    pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
    pos = dns_packet_copy(buf,len,pos,header,10); if (!pos) return -1;
    uint16_unpack_big(header + 8,&datalen);
    if (byte_equal(header,2,DNS_T_A))
      if (byte_equal(header + 2,2,DNS_C_IN))
        if (datalen == 4) {
	  if (!dns_packet_copy(buf,len,pos,header,4)) return -1;
	  if (!stralloc_catb(out,header,4)) return -1;
          count++;
	}
    pos += datalen;
  }

  if (!count) return -1;
  dns_sortip(out->s,out->len);
  return 0;
}

static char *q = 0;

int dns_ip4(stralloc *out,const stralloc *fqdn,const int timeout)
{
  unsigned int i;
  char code;
  char ch;

  if (!stralloc_copys(out,"")) return -1;
  code = 0;
  for (i = 0;i <= fqdn->len;++i) {
    if (i < fqdn->len)
      ch = fqdn->s[i];
    else
      ch = '.';

    if ((ch == '[') || (ch == ']')) continue;
    if (ch == '.') {
      if (!stralloc_append(out,&code)) return -1;
      code = 0;
      continue;
    }
    if ((ch >= '0') && (ch <= '9')) {
      code *= 10;
      code += ch - '0';
      continue;
    }

    if (!dns_domain_fromdot(&q,fqdn->s,fqdn->len)) return -1;
    if (dns_resolve(q,DNS_T_A,4,timeout) == -1) return -1;
    if (dns_ip4_packet(out,dns_resolve_tx.packet,dns_resolve_tx.packetlen) == -1) return -1;
    dns_transmit_free(&dns_resolve_tx);
    dns_domain_free(&q);
    return 0;
  }

  out->len &= ~3;
  return 0;
}
