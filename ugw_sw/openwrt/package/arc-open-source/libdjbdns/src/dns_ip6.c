#include "stralloc.h"
#include "uint16.h"
#include "byte.h"
#include "dns.h"
#include "ip4.h"
#include "ip6.h"
#include <sys/types.h>
#include <unistd.h>


#if 1
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <stdio.h>

#define FONAP_RUN_DIR   "/tmp/fonap"
#define FONAP_LOG_SIZE  100000
#define FONAP_LOG_FLAG  FONAP_RUN_DIR "/fonap_debug_flag"
#define FONAP_FONSMCD_DEBUG_LOG_FILE    FONAP_RUN_DIR "/fonsmcd_debug.log"

#define FONAP_DEBUG(fmt, arg...) do {\
  FILE *fp; \
  struct stat stat_buf; \
  if ((stat(FONAP_LOG_FLAG, &stat_buf) == 0) && S_ISREG(stat_buf.st_mode)) { \
    if ((stat(FONAP_FONSMCD_DEBUG_LOG_FILE, &stat_buf) == 0) && (stat_buf.st_size < FONAP_LOG_SIZE)) { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "a"); \
    } else { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "w"); \
    } \
    if (fp) { \
      FILE *uptime_fp; \
      unsigned long up_s = 0; \
      unsigned long up_ns = 0; \
      unsigned long id_s = 0; \
      unsigned long id_ns = 0; \
      uptime_fp=fopen("/proc/uptime", "r"); \
      if (uptime_fp) { \
        fscanf(uptime_fp, "%lu.%02lu %lu.%02lu", &up_s, &up_ns, &id_s, &id_ns); \
        fclose(uptime_fp); \
      } \
      fprintf(fp, "[uptime:%lu.%02lu]: pid=[%d] ", up_s, up_ns, getpid()); \
      fprintf(fp, fmt, ## arg); \
      fprintf(fp, "\n"); \
      fclose(fp); \
    } \
  } \
} while (0)
#else
#define FONAP_DEBUG(fmt, arg...)
#endif

static int dns_ip6_packet_add(stralloc *out,char *buf,unsigned int len)
{
  unsigned int pos;
  char header[16];
  uint16 numanswers;
  uint16 datalen;
  int count = 0;

  pos = dns_packet_copy(buf,len,0,header,12); if (!pos) return -1;
  uint16_unpack_big(header + 6,&numanswers);
  pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
  pos += 4;

  while (numanswers--) {
    pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
    pos = dns_packet_copy(buf,len,pos,header,10); if (!pos) return -1;
    uint16_unpack_big(header + 8,&datalen);
    if (byte_equal(header,2,DNS_T_AAAA)) {
      if (byte_equal(header + 2,2,DNS_C_IN))
        if (datalen == 16) {
	  if (!dns_packet_copy(buf,len,pos,header,16)) return -1;
	  if (!stralloc_catb(out,header,16)) return -1;
          count++;
	}
    }
/* FIXME: we don't try DNS_T_A right now. */
#if 0
    else if (byte_equal(header,2,DNS_T_A)) {
      if (byte_equal(header + 2,2,DNS_C_IN))
        if (datalen == 4) {
	  byte_copy(header,12,V4mappedprefix);
	  if (!dns_packet_copy(buf,len,pos,header+12,4)) return -1;
	  if (!stralloc_catb(out,header,16)) return -1;
	}
    }
#endif
    pos += datalen;
  }
  if (!count) return -1;
  dns_sortip6(out->s,out->len);
  return 0;
}

int dns_ip6_packet(stralloc *out,char *buf,unsigned int len) {
  if (!stralloc_copys(out,"")) return -1;
  return dns_ip6_packet_add(out,buf,len);
}

static char *q = 0;

int dns_ip6(stralloc *out,stralloc *fqdn,const int timeout)
{
  unsigned int i;
  char code;
  char ch;
  char ip[16];

  if (!stralloc_copys(out,"")) return -1;
  if (!stralloc_readyplus(fqdn,1)) return -1;
  fqdn->s[fqdn->len]=0;
  if ((i=ip6_scan(fqdn->s,ip))) {
    if (fqdn->s[i]) return -1;
    stralloc_copyb(out,ip,16);
    return 0;
  }
  code = 0;
  for (i = 0;i <= fqdn->len;++i) {
    if (i < fqdn->len)
      ch = fqdn->s[i];
    else
      ch = '.';

    if ((ch == '[') || (ch == ']')) continue;
    if (ch == '.') {
      if (!stralloc_append(out,&code)) return -1;
      code = 0;
      continue;
    }
    if ((ch >= '0') && (ch <= '9')) {
      code *= 10;
      code += ch - '0';
      continue;
    }

    if (!dns_domain_fromdot(&q,fqdn->s,fqdn->len)) { FONAP_DEBUG("%s(%d)", __func__, __LINE__); return -1; }
    if (dns_resolve(q,DNS_T_AAAA,6,timeout) == -1) { FONAP_DEBUG("%s(%d)", __func__, __LINE__); return -1; }
    if (dns_ip6_packet(out,dns_resolve_tx.packet,dns_resolve_tx.packetlen) == -1) { FONAP_DEBUG("%s(%d)", __func__, __LINE__); return -1; }
    FONAP_DEBUG("%s(%d)", __func__, __LINE__);
    dns_transmit_free(&dns_resolve_tx);
    dns_domain_free(&q);
    return 0;
  }

  out->len &= ~3;
  return 0;
}
