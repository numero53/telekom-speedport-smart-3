#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "socket.h"
#include "alloc.h"
#include "error.h"
#include "byte.h"
#include "uint16.h"
#include "dns.h"
#include "ip6.h"

//#include "fonap.h"

#if 1
#include <sys/sysinfo.h>
#include <sys/stat.h>

#define FONAP_RUN_DIR   "/tmp/fonap"
#define FONAP_LOG_SIZE  100000
#define FONAP_LOG_FLAG  FONAP_RUN_DIR "/fonap_debug_flag"
#define FONAP_FONSMCD_DEBUG_LOG_FILE    FONAP_RUN_DIR "/fonsmcd_debug.log"

#define FONAP_DEBUG(fmt, arg...) do {\
  FILE *fp; \
  struct stat stat_buf; \
  if ((stat(FONAP_LOG_FLAG, &stat_buf) == 0) && S_ISREG(stat_buf.st_mode)) { \
    if ((stat(FONAP_FONSMCD_DEBUG_LOG_FILE, &stat_buf) == 0) && (stat_buf.st_size < FONAP_LOG_SIZE)) { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "a"); \
    } else { \
      fp=fopen(FONAP_FONSMCD_DEBUG_LOG_FILE, "w"); \
    } \
    if (fp) { \
      FILE *uptime_fp; \
      unsigned long up_s = 0; \
      unsigned long up_ns = 0; \
      unsigned long id_s = 0; \
      unsigned long id_ns = 0; \
      uptime_fp=fopen("/proc/uptime", "r"); \
      if (uptime_fp) { \
        fscanf(uptime_fp, "%lu.%02lu %lu.%02lu", &up_s, &up_ns, &id_s, &id_ns); \
        fclose(uptime_fp); \
      } \
      fprintf(fp, "[uptime:%lu.%02lu]: pid=[%d] ", up_s, up_ns, getpid()); \
      fprintf(fp, fmt, ## arg); \
      fprintf(fp, "\n"); \
      fclose(fp); \
    } \
  } \
} while (0)
#else
#define FONAP_DEBUG(fmt, arg...)
#endif

/****************/
/* IPv6 trigger */
/****************/
#if 1
#define PATH_TMP_IPV6_REJECT    "/tmp/arc_ipv6_reject" 
static int arc_wan_ip6_on_demand(void)
{
	FILE *fp;
	char cmd[64];
	char buf[4];
	int ret = -1;

	if ((fp = fopen(PATH_TMP_IPV6_REJECT, "r"))) {
		if (fgets(buf, sizeof(buf), fp)) {
			FONAP_DEBUG("%s %s [%s]", __FUNCTION__, PATH_TMP_IPV6_REJECT, buf);
			if (isdigit(buf[0])) {
				ret = atoi(buf);
			}
		}
		fclose(fp);
	}
	if (ret == 1) {
		FONAP_DEBUG("%s WAN IPv6 connection reject.", __FUNCTION__);
		return 2;
	}

	/* clean ret */
	ret = -1;

	/* active_wan_type@system, 0 means ADSL */
	/* active_wan_type@system, 1 means ethernet */
	/* active_wan_type@system, 2 means VDSL */
	if ((fp = popen("mng_cli get ARC_WAN_Type", "r"))) {
		if (fgets(buf, sizeof(buf), fp)) {
			FONAP_DEBUG("%s active WAN type [%s]", __FUNCTION__, buf);
			if (isdigit(buf[0])) {
				ret = atoi(buf);
			}
		}
		else {
			FONAP_DEBUG("%s command %s error.", __FUNCTION__, "mng_cli get ARC_WAN_Type");
		}
		pclose(fp);
	}

	if (ret == 0) {
		snprintf(cmd, sizeof(cmd), "abscfg get ARC_WAN_xy_PPP_OnDemand %d 0", ret);
	}
	else if (ret == 1) {
		snprintf(cmd, sizeof(cmd), "abscfg get ARC_WAN_xy_PPP_OnDemand %d 0", ret);
	}
	else if (ret == 2) {
		snprintf(cmd, sizeof(cmd), "abscfg get ARC_WAN_xy_PPP_OnDemand %d 0", ret);
	}
	else {
		FONAP_DEBUG("%s Unknown WAN type %d error.", __FUNCTION__, ret);
		return -1;
	}

	/* clean ret */
	ret = -1;

	/* on-demand@wan050, 0 means pppoe ipv4 keep-alive. */
	/* on-demand@wan050, 1 means pppoe ipv4 on-demand */
	if ((fp = popen(cmd, "r"))) {
		if (fgets(buf, sizeof(buf), fp)) {
			FONAP_DEBUG("%s WAN on-demand [%s]", __FUNCTION__, buf);
			if (isdigit(buf[0])) {
				ret = atoi(buf);
			}
		}
		else {
			FONAP_DEBUG("%s command %s error.", __FUNCTION__, cmd);
		}
		pclose(fp);
	}

	FONAP_DEBUG("%s ret=[%d]", __FUNCTION__, ret);
	return ret;
}

static int arc_wan_ip6_status(void)
{
	FILE *fp;
	char buf[4];
	int ret = -1;

	/* ip6_wan_ready, 0 means pppoe ipv6 disconnected; 1 means pppoe ipv6 connected. */
	/* ip6_wan_ready, 2 means pppoe ipv6 connecting */
	if ((fp = popen("abscfg get ARC_WAN_TMP_DefaultRoute_IPv6_WAN_Ready", "r"))) {
		if (fgets(buf, sizeof(buf), fp)) {
			FONAP_DEBUG("%s WAN IPv6 status [%s]", __FUNCTION__, buf);
			if (isdigit(buf[0])) {
				ret = atoi(buf);
			}
		}
		else {
			FONAP_DEBUG("%s command %s error.", __FUNCTION__, "abscfg get ARC_WAN_TMP_DefaultRoute_IPv6_WAN_Ready");
		}
		pclose(fp);
	}

	FONAP_DEBUG("%s ret=[%d]", __FUNCTION__, ret);
	return ret;
}

#define ARC_WAN_IP6_TRIGGER_TIMEOUT	10

static int arc_var_wan_ip6_on_demand = -1;
static int dns_wan_ip6_up(struct dns_transmit *d)
{
	int on_demand, ret, i;

	/* on-demand, 0 means pppoe ipv6 keep-alive. */
	/* on-demand, 1 means pppoe ipv6 on-demand */
	/* on-demand, 2 means pppoe ipv6 connection reject */
	if (arc_var_wan_ip6_on_demand == -1) {
		/* init it */
		arc_var_wan_ip6_on_demand = arc_wan_ip6_on_demand();
	}
	else if (arc_var_wan_ip6_on_demand == 1) {
		/* check if it been rejected */
		arc_var_wan_ip6_on_demand = arc_wan_ip6_on_demand();
	}
	on_demand = arc_var_wan_ip6_on_demand;

	/* ip6_wan_ready, 0 means pppoe ipv6 disconnected; 1 means pppoe ipv6 connected. */
	/* ip6_wan_ready, 2 means pppoe ipv6 connecting */
	ret = arc_wan_ip6_status();
	if (on_demand == 1) {
		if (ret == 0) {
			/* WAN IPv6 idle, trigger IPV6CP */
			system("arc_trigger 6");
			for (i = ARC_WAN_IP6_TRIGGER_TIMEOUT; i > 0; i--) {
				sleep(1);
				ret = arc_wan_ip6_status();
				if (ret == 1) {
					/* connected now */
					i = 0;
				}
				else if (ret == 2) {
					/* connecting now */
					i = 0;
				}
			}
		}

		/* check if we are connecting? */
		if (ret == 2) {
			/* WAN IPv6 triggering now, wait 5 seconds */
			for (i = ARC_WAN_IP6_TRIGGER_TIMEOUT; i > 0; i--) {
				sleep(1);
				ret = arc_wan_ip6_status();
				if (ret == 1) {
					/* connected now */
					i = 0;
				}
				else if (ret == 0) {
					/* disconnected now */
					i = 0;
				}
			}
		}
	}

	if (ret == 1)
		return 1;
	else
		return 0;
}
#endif

static int serverwantstcp(const char *buf,unsigned int len)
{
  char out[12];

  if (!dns_packet_copy(buf,len,0,out,12)) return 1;
  if (out[2] & 2) return 1;
  return 0;
}

static int serverfailed(const char *buf,unsigned int len)
{
  char out[12];
  unsigned int rcode;

  if (!dns_packet_copy(buf,len,0,out,12)) return 1;
  rcode = out[3];
  rcode &= 15;
  if (rcode && (rcode != 3)) { errno = error_again; return 1; }
  return 0;
}

static int irrelevant(const struct dns_transmit *d,const char *buf,unsigned int len)
{
  char out[12];
  char *dn;
  unsigned int pos;

  pos = dns_packet_copy(buf,len,0,out,12); if (!pos) return 1;
  if (byte_diff(out,2,d->query + 2)) return 1;
  if (out[4] != 0) return 1;
  if (out[5] != 1) return 1;

  dn = 0;
  pos = dns_packet_getname(buf,len,pos,&dn); if (!pos) return 1;
  if (!dns_domain_equal(dn,d->query + 14)) { alloc_free(dn); return 1; }
  alloc_free(dn);

  pos = dns_packet_copy(buf,len,pos,out,4); if (!pos) return 1;
  if (byte_diff(out,2,d->qtype)) return 1;
  if (byte_diff(out + 2,2,DNS_C_IN)) return 1;

  return 0;
}

static void packetfree(struct dns_transmit *d)
{
  if (!d->packet) return;
  alloc_free(d->packet);
  d->packet = 0;
}

static void queryfree(struct dns_transmit *d)
{
  if (!d->query) return;
  alloc_free(d->query);
  d->query = 0;
}

static void socketfree(struct dns_transmit *d)
{
  if (!d->s1) return;
  close(d->s1 - 1);
  d->s1 = 0;
}

#if 0
void dns_transmit_free(struct dns_transmit *d)
{
  queryfree(d);
  socketfree(d);
  packetfree(d);
}
#endif

static int randombind(struct dns_transmit *d)
{
  int j;

  for (j = 0;j < 10;++j)
    if (socket_bind6(d->s1 - 1,d->localip,1025 + dns_random(64510),d->scope_id) == 0)
      return 0;
  if (socket_bind6(d->s1 - 1,d->localip,0,d->scope_id) == 0)
    return 0;
  return -1;
}

static const int timeouts[4] = { 1, 3, 11, 45 };

static int thisudp_at_port(struct dns_transmit *d, int port)
{
  const char *ip;

  socketfree(d);

  //while (d->udploop < 4) {
  while (d->udploop < 2) {
    for (;d->curserver < 16;++d->curserver) {
      ip = d->servers + 16 * d->curserver;
      if (byte_diff(ip,16,V6any)) {
        /* first check if interface is ready ? */
#if 1
        if (!dns_wan_ip6_up(d)) {
          FONAP_DEBUG("%s WAN Interface IPv6 can't be up", __FUNCTION__);
          break;
        }
#else
        if (fonap_ip6_wan_ready(FONAP_IP6_WAN_READY_WAIT_TIME, 1) != RET_OK) {
          FONAP_DEBUG("%s WAN Interface IPv6 can't be up", __FUNCTION__);
          break;
        }
#endif
	d->query[2] = dns_random(256);
	d->query[3] = dns_random(256);
  
        d->s1 = 1 + socket_udp6();
        if (!d->s1) { dns_transmit_free(d); return -1; }
	if (randombind(d) == -1) { dns_transmit_free(d); return -1; }

        if (socket_connect6(d->s1 - 1,ip,port,d->scope_id) == 0)
          if (send(d->s1 - 1,d->query + 2,d->querylen - 2,0) == d->querylen - 2) {
            struct taia now;
            taia_now(&now);
            FONAP_DEBUG("%s(%d) d->udploop=[%d] timeouts[d->udploop]=[%d]", __func__, __LINE__, d->udploop, timeouts[d->udploop]);
            taia_uint(&d->deadline,timeouts[d->udploop]);
            taia_add(&d->deadline,&d->deadline,&now);
            d->tcpstate = 0;
            return 0;
          }
  
        socketfree(d);
      }
    }

    ++d->udploop;
    d->curserver = 0;
  }

  dns_transmit_free(d); return -1;
}

#if 0
static int thisudp(struct dns_transmit *d)
{
#if 0
  return thisudp_at_port(d, 53);
#else
  return thisudp_at_port(d, DNS_DEFAULT_PORT);
#endif
}
#endif

static int firstudp_at_port(struct dns_transmit *d, int port)
{
  d->curserver = 0;
  FONAP_DEBUG("%s(%d) enter. d->curserver=[%d]", __func__, __LINE__, d->curserver);
  return thisudp_at_port(d, port);
}

#if 0
static int firstudp(struct dns_transmit *d)
{
#if 0
  return firstudp_at_port(d, 53);
#else
  return firstudp_at_port(d, DNS_DEFAULT_PORT);
#endif
}
#endif

static int nextudp_at_port(struct dns_transmit *d, int port)
{
  ++d->curserver;
  FONAP_DEBUG("%s(%d) enter. d->curserver=[%d]", __func__, __LINE__, d->curserver);
  return thisudp_at_port(d, port);
}

static int nextudp(struct dns_transmit *d)
{
#if 0
  return nextudp_at_port(d, 53);
#else
  return nextudp_at_port(d, DNS_DEFAULT_PORT);
#endif
}

static int thistcp_at_port(struct dns_transmit *d, int port)
{
  struct taia now;
  const char *ip;

  socketfree(d);
  packetfree(d);

  for (;d->curserver < 16;++d->curserver) {
    ip = d->servers + 16 * d->curserver;
    if (byte_diff(ip,16,V6any)) {
      d->query[2] = dns_random(256);
      d->query[3] = dns_random(256);

      d->s1 = 1 + socket_tcp6();
      if (!d->s1) { dns_transmit_free(d); return -1; }
      if (randombind(d) == -1) { dns_transmit_free(d); return -1; }
  
      taia_now(&now);
      taia_uint(&d->deadline,10);
      taia_add(&d->deadline,&d->deadline,&now);
      if (socket_connect6(d->s1 - 1,ip,port,d->scope_id) == 0) {
        d->tcpstate = 2;
        return 0;
      }
      if ((errno == error_inprogress) || (errno == error_wouldblock)) {
        d->tcpstate = 1;
        return 0;
      }
  
      socketfree(d);
    }
  }

  dns_transmit_free(d); return -1;
}

static int firsttcp_at_port(struct dns_transmit *d, int port)
{
  d->curserver = 0;
  return thistcp_at_port(d, port);
}

static int firsttcp(struct dns_transmit *d)
{
#if 0
  return firsttcp_at_port(d, 53);
#else
  return firsttcp_at_port(d, DNS_DEFAULT_PORT);
#endif
}

static int nexttcp_at_port(struct dns_transmit *d, int port)
{
  ++d->curserver;
  return thistcp_at_port(d, port);
}

static int nexttcp(struct dns_transmit *d)
{
#if 0
  return nexttcp_at_port(d, 53);
#else
  return nexttcp_at_port(d, DNS_DEFAULT_PORT);
#endif
}

int dns_transmit6_start_at_port(struct dns_transmit *d,const char servers[64],int flagrecursive,const char *q,const char qtype[2],const char localip6[16], int port)
{
  unsigned int len;

  FONAP_DEBUG("%s(%d) enter.", __func__, __LINE__);

  dns_transmit_free(d);
  errno = error_io;

  len = dns_domain_length(q);
  d->querylen = len + 18;
  d->query = alloc(d->querylen);
  if (!d->query) return -1;

  uint16_pack_big(d->query,len + 16);
  byte_copy(d->query + 2,12,flagrecursive ? "\0\0\1\0\0\1\0\0\0\0\0\0" : "\0\0\0\0\0\1\0\0\0\0\0\0gcc-bug-workaround");
  byte_copy(d->query + 14,len,q);
  byte_copy(d->query + 14 + len,2,qtype);
  byte_copy(d->query + 16 + len,2,DNS_C_IN);

  byte_copy(d->qtype,2,qtype);
  d->servers = servers;
  d->ipversion = 6;
  byte_copy(d->localip6,16,localip6);

  d->udploop = flagrecursive ? 1 : 0;

  if (len + 16 > 512) return firsttcp_at_port(d, port);
  return firstudp_at_port(d, port);
}

int dns_transmit6_start(struct dns_transmit *d,const char servers[64],int flagrecursive,const char *q,const char qtype[2],const char localip6[16])
{
#if 0
  return dns_transmit6_start_at_port(d, servers, flagrecursive, q, qtype, localip6, 53);
#else
  return dns_transmit6_start_at_port(d, servers, flagrecursive, q, qtype, localip6, DNS_DEFAULT_PORT);
#endif
}

int dns_transmit6_get(struct dns_transmit *d,const iopause_fd *x,const struct taia *when)
{
  char udpbuf[513];
  unsigned char ch;
  int r;
  int fd;

  errno = error_io;
  fd = d->s1 - 1;

  if (!x->revents) {
    if (taia_less(when,&d->deadline)) return 0;
    errno = error_timeout;
    if (d->tcpstate == 0) return nextudp(d);
    return nexttcp(d);
  }

  if (d->tcpstate == 0) {
/*
have attempted to send UDP query to each server udploop times
have sent query to curserver on UDP socket s
*/
    r = recv(fd,udpbuf,sizeof udpbuf,0);
    if (r <= 0) {
/* FIXME: hue modify to not re-send DNS query again */
#if 0
      if (errno == error_connrefused) if (d->udploop == 2) return 0;
      return nextudp(d);
#else
      return 0;
#endif
    }
    if (r + 1 > sizeof udpbuf) return 0;

    if (irrelevant(d,udpbuf,r)) return 0;
    if (serverwantstcp(udpbuf,r)) return firsttcp(d);
    if (serverfailed(udpbuf,r)) {
/* FIXME: hue modify to not re-send DNS query again */
#if 0
      if (d->udploop == 2) return 0;
      return nextudp(d);
#else
      return 0;
#endif
    }
    socketfree(d);

    d->packetlen = r;
    d->packet = alloc(d->packetlen);
    if (!d->packet) { dns_transmit_free(d); return -1; }
    byte_copy(d->packet,d->packetlen,udpbuf);
    queryfree(d);
    return 1;
  }

  if (d->tcpstate == 1) {
/*
have sent connection attempt to curserver on TCP socket s
pos not defined
*/
    if (!socket_connected6(fd)) return nexttcp(d);
    d->pos = 0;
    d->tcpstate = 2;
    return 0;
  }

  if (d->tcpstate == 2) {
/*
have connection to curserver on TCP socket s
have sent pos bytes of query
*/
    r = write(fd,d->query + d->pos,d->querylen - d->pos);
    if (r <= 0) return nexttcp(d);
    d->pos += r;
    if (d->pos == d->querylen) {
      struct taia now;
      taia_now(&now);
      taia_uint(&d->deadline,10);
      taia_add(&d->deadline,&d->deadline,&now);
      d->tcpstate = 3;
    }
    return 0;
  }

  if (d->tcpstate == 3) {
/*
have sent entire query to curserver on TCP socket s
pos not defined
*/
    r = read(fd,&ch,1);
    if (r <= 0) return nexttcp(d);
    d->packetlen = ch;
    d->tcpstate = 4;
    return 0;
  }

  if (d->tcpstate == 4) {
/*
have sent entire query to curserver on TCP socket s
pos not defined
have received one byte of packet length into packetlen
*/
    r = read(fd,&ch,1);
    if (r <= 0) return nexttcp(d);
    d->packetlen <<= 8;
    d->packetlen += ch;
    d->tcpstate = 5;
    d->pos = 0;
    d->packet = alloc(d->packetlen);
    if (!d->packet) { dns_transmit_free(d); return -1; }
    return 0;
  }

  if (d->tcpstate == 5) {
/*
have sent entire query to curserver on TCP socket s
have received entire packet length into packetlen
packet is allocated
have received pos bytes of packet
*/
    r = read(fd,d->packet + d->pos,d->packetlen - d->pos);
    if (r <= 0) return nexttcp(d);
    d->pos += r;
    if (d->pos < d->packetlen) return 0;

    socketfree(d);
    if (irrelevant(d,d->packet,d->packetlen)) return nexttcp(d);
    if (serverwantstcp(d->packet,d->packetlen)) return nexttcp(d);
    if (serverfailed(d->packet,d->packetlen)) return nexttcp(d);

    queryfree(d);
    return 1;
  }

  return 0;
}
