#ifndef SOCKET_H
#define SOCKET_H

#include "uint16.h"
#include "uint32.h"

#define DT_MARK_LAN2WAN         0x0001U
#define DT_MARK_RESERVE_CONN    0x0002U
#define DT_MARK_FON             0x0010U

extern int socket_tcp(void);
extern int socket_udp(void);
extern int socket_tcp6(void);
extern int socket_udp6(void);

extern int socket_connect4(int,const char *,uint16);
extern int socket_connect6(int,const char *,uint16,uint32);
extern int socket_connected(int);
extern int socket_connected6(int);
extern int socket_bind4(int,char *,uint16);
extern int socket_bind6(int,char *,uint16,uint32);
extern int socket_bind4_reuse(int,char *,uint16);
extern int socket_bind6_reuse(int,char *,uint16,uint32);
extern int socket_listen(int,int);
extern int socket_accept4(int,char *,uint16 *);
extern int socket_recv4(int,char *,int,char *,uint16 *);
extern int socket_send4(int,const char *,int,const char *,uint16);
extern int socket_local4(int,char *,uint16 *);
extern int socket_remote4(int,char *,uint16 *);

extern void socket_tryreservein(int,int);

#endif
