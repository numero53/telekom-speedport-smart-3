#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "ndelay.h"
#include "socket.h"

int socket_tcp6(void)
{
  int s;
  unsigned int dt_mark = DT_MARK_FON;

  s = socket(AF_INET6,SOCK_STREAM,0);
  if (s == -1) return -1;
  if (ndelay_on(s) == -1) { close(s); return -1; }
  if (setsockopt(s, SOL_SOCKET, SO_MARK, &dt_mark, sizeof(unsigned int)) == -1) { close(s); return -1; }
  return s;
}
