#!/bin/sh
#
# Copyright (C) 2009 Arcadyan
# All Rights Reserved.

#mail_return_addr=guest@xxx.yyy.zzz
#smtp_addr=111.222.333.444
#Mail_POP_Server=222.333.444.555
#Mail_Username=1234
#Mail_Password=5678
#Mail_TLSSUPP=1
#Mail_SMTPPORT=25
#-----------------
#Root=jayfan.us@gmail.com
#MailHub=smtp.gmail.com:587
#HostName=smtp.gmail.com:587
#RewriteDomain=
#FromLineOverride=YES
#AuthUser=jayfan.us
#AuthPass=quizquiz
#UseSTARTTLS=YES

CMD=""
tmpS=""
SSMTP_FILE="/etc/config/ssmtp/ssmtp.conf"

new_cmdCFG(){
	local cfg_file=$1
	local cmd=$2
	local index=$3
#index -1:last
#    if [ $index -eq -1 ]; then
	echo -e "$cmd" >> $cfg_file
#    else
#               for(($curr=0;$curr<=$index;$curr=$curr+1))
#               do
#               sed -i 's/#'"${curr}"'/'"${cmd}"' #'"${index}"''
#       done
#    fi

	return 0
}

rulelist=`abscfg get ARC_SSMTP_Rule_List`
rulelist=`echo $rulelist|tr -s ',' ' '`
rulenum=`echo ${rulelist} | awk '{print NF}'`
#echo ${rulelist} > /dev/console
#echo ${rulenum} > /dev/console

if [ ${rulenum} -gt 1 ]; then
	for idxRule in $rulelist;
	do
		CMD=""
		SSMTP_FILE_IDX=${SSMTP_FILE}_${idxRule}
		rm ${SSMTP_FILE_IDX}
		opt_value=`abscfg get ARC_SSMTP_x_Mail_Addr ${idxRule}`
		if [ -n "$opt_value"  ]; then
			CMD=$CMD"Root="$opt_value"\n"
		fi
		opt_value=`abscfg get ARC_SSMTP_x_Smtp_Server ${idxRule}`
		if [ -n "$opt_value"  ]; then
			CMD=$CMD"HostName="$opt_value"\n"
			tmpS=$opt_value
		fi
		opt_value=`abscfg get ARC_SSMTP_x_Smtp_Port ${idxRule}`
		if [ -n "$opt_value"  ]; then
			tmpS=$tmpS":"$opt_value
		fi
		CMD=$CMD"MailHub="$tmpS"\n"
		#CMD=$CMD"RewriteDomain=\n"
		CMD=$CMD"FromLineOverride=YES\n"

		#email_provider=`ccfg_cli get email_provider@nas`
		#if [ "$email_provider" == "1" ]; then
		#	#echo email_provider = $email_provider > /dev/console
		#	user_id=`ccfg_cli get email_other_username@nas`
		#	if [ `echo $user_id | grep -c "hotmail.com"` -gt 0 ]; then
		#		CMD=$CMD"AuthUser="$user_id"\n"
				#echo AuthUser1 = $user_id > /dev/console
		#	elif [ `echo $user_id | grep -c "outlook.com"` -gt 0 ]; then
		#		CMD=$CMD"AuthUser="$user_id"\n"
				#echo AuthUser2 = $user_id > /dev/console
		#	else
		#		opt_value=$(get_option_value "UserName" $idxRule )
		#		if [ -n "$opt_value"  ]; then
		#			CMD=$CMD"AuthUser="$opt_value"\n"
		#		fi
		#	fi
		#else
			opt_value=`abscfg get ARC_SSMTP_x_User_Name ${idxRule}`
			if [ -n "$opt_value"  ]; then
				CMD=$CMD"AuthUser="$opt_value"\n"
			fi
		#fi
		opt_value=`abscfg get ARC_SSMTP_x_Password ${idxRule}`
		if [ -n "$opt_value"  ]; then
			CMD=$CMD"AuthPass="$opt_value"\n"
		fi

		opt_value=`abscfg get ARC_SSMTP_x_Tls_Supp ${idxRule}`
		if [ -n "$opt_value" -a $opt_value -gt 0 ]; then
			CMD=$CMD"UseSTARTTLS=YES\n"
		fi

		#if [ "$email_provider" == "1" ]; then
		#	user_id=`ccfg_cli get email_other_username@nas`
		#	CMD=$CMD"From="$user_id"\n"
		#else
		#	user_id=`ccfg_cli get email_t_username@nas`
		#	user_id="$user_id""@t-online.de"
		#	CMD=$CMD"From="$user_id"\n"
		#fi
		#echo "$CMD"
		new_cmdCFG $SSMTP_FILE_IDX "$CMD"
	done
else
	rm $SSMTP_FILE

	email_provider=`abscfg get ARC_PROJ_EPS_EmailProvider`

	if [ "$email_provider" == "1" ]; then
		opt_value=`abscfg get ARC_PROJ_EPS_Email_Other_UserName`
		if [ -n "$opt_value" ]; then
			CMD=$CMD"Root="$opt_value"\n"
		fi
	else
		user_id=`abscfg get ARC_PROJ_EPS_Email_T_UserName`
		user_id="$user_id""@t-online.de"
		CMD=$CMD"Root="$user_id"\n"	
	fi

	opt_value=`abscfg get ARC_SSMTP_x_Smtp_Server 0`
	if [ -n "$opt_value" ]; then
		CMD=$CMD"HostName="$opt_value"\n"
		tmpS=$opt_value
	fi

	opt_value=`abscfg get ARC_SSMTP_x_Smtp_Port 0`
	if [ -n "$opt_value" ]; then
		tmpS=$tmpS":"$opt_value
	fi

	CMD=$CMD"MailHub="$tmpS"\n"
	#CMD=$CMD"RewriteDomain=\n"
	CMD=$CMD"FromLineOverride=YES\n"

	if [ "$email_provider" == "1" ]; then
		opt_value=`abscfg get ARC_PROJ_EPS_Email_Other_UserName`
		if [ -n "$opt_value" ]; then
			CMD=$CMD"AuthUser="$opt_value"\n"
		fi

		opt_value=`abscfg get ARC_PROJ_EPS_Email_Other_Password`
		if [ -n "$opt_value" ]; then
			CMD=$CMD"AuthPass="$opt_value"\n"
		fi
	else
		opt_value=`abscfg get ARC_PROJ_EPS_Email_T_UserName`
		if [ -n "$opt_value" ]; then
			CMD=$CMD"AuthUser="$opt_value"\n"
		fi

		opt_value=`abscfg get ARC_PROJ_EPS_Email_T_Password`
		if [ -n "$opt_value" ]; then
			CMD=$CMD"AuthPass="$opt_value"\n"
		fi
	fi

	opt_value=`abscfg get ARC_SSMTP_x_Smtp_Port 0`
	if [ $opt_value == "465" ]; then
		CMD=$CMD"UseTLS=YES\n"
	elif [ $opt_value == "587" ]; then
		CMD=$CMD"UseSTARTTLS=YES\n"
		CMD=$CMD"UseTLS=YES\n"
	elif [ $opt_value == "25" ]; then
		CMD=$CMD"UseSTARTTLS=YES\n"
	fi

	if [ "$email_provider" == "1" ]; then
		user_id=`abscfg get ARC_PROJ_EPS_Email_Other_UserName`
		CMD=$CMD"From="$user_id"\n"
	else
		user_id=`abscfg get ARC_PROJ_EPS_Email_T_UserName`
		user_id="$user_id""@t-online.de"
		CMD=$CMD"From="$user_id"\n"
	fi

	#CMD=$CMD"Debug=YES\n"
	#echo "$CMD"
	new_cmdCFG $SSMTP_FILE "$CMD"

fi

