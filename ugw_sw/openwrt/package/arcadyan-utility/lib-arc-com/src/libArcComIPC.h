void* TCP_Socket_open(const char *serv_path, const char *clnt_path);
void* UDP_Socket_open(const char *serv_path, const char *clnt_path);
void Socket_close(void *sock_info);

int Socket_send(const void *sock_info, const void *send_buf, const unsigned int send_len);
int Socket_recv(const void *sock_info, void *recv_buf, const unsigned int recv_len);
int Socket_recv_PEEK(const void *sock_info, void *recv_buf, const unsigned int recv_len);

int TCP_Socket_send_recv_once(const char *serv_path, const void *send_buf, const unsigned int	send_len, const char *clnt_path, void *recv_buf, const unsigned int	recv_len);
int UDP_Socket_send_recv_once(const char *serv_path, const void *send_buf, const unsigned int	send_len, const char *clnt_path, void *recv_buf, const unsigned int	recv_len);