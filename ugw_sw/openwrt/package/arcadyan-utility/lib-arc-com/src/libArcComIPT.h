#ifndef _ARC_COM_IPT_H_
#define _ARC_COM_IPT_H_


#include <stddef.h>

#define _DBG_LV				0
#if _DBG_LV > 0
#define dbg_printf(lv, fmt, arg...) do {if(lv<_DBG_LV) printf(fmt, ##arg);} while(0)
#else
#define dbg_printf(lv, fmt, arg...)
#endif

#define INT_TABLE_RAW		0
#define INT_TABLE_NAT		1
#define INT_TABLE_MANGLE	2
#define INT_TABLE_FILTER	3
#define INT_TABLE_NUM		4

#define INT_CMDACT_INS		0
#define INT_CMDACT_APPEND	1
#define INT_CMDACT_NEW		2

#define WAIT_FW3_LIMIT		10
#define WAIT_FW3_TIME		1
#define FW3_LOCKFILE		"/var/run/fw3.lock" //This filename should be as same as FW3_LOCKFILE in build_dir/target-xxxxx/firewall-[version]/utils.h 

#define LIBIPT_VER_V4		0x01
#define LIBIPT_VER_V6		0x02
#define LIBIPT_VER_ANY		0x03

#define LEN_MAX_CMD_IPT		500
#define LEN_SHORT_CMD_IPT	50

#define RET_SUCCESS				0 //SUCCESS

#define RET_FAIL_PARAMS			2
#define RET_FAIL_FILEOPEN		5
#define RET_FAIL_FILELOCK		7

//define the structure of the out put restore file
struct cmdItem_t{
	char cmd[LEN_MAX_CMD_IPT];
	TAILQ_ENTRY(cmdItem_t) cmdItems;
};
struct cmdbuf_t{
	//chains
	TAILQ_HEAD(chain_list, cmdItem_t) chains[INT_TABLE_NUM];
	//rules
	TAILQ_HEAD(rule_list, cmdItem_t) rules[INT_TABLE_NUM];
};

char start_iptcmd(struct cmdbuf_t *);
char add_iptcmd(struct cmdbuf_t *, char *, ...);
void dump_iptcmd(struct cmdbuf_t *);
char write_iptcmd(char *, struct cmdbuf_t *);
char write_del_iptcmd(char *, struct cmdbuf_t *);
char write_ipt6cmd(char *filepath, struct cmdbuf_t *cmds);
char exec_iptcmd(__uint8_t , char *);
char end_iptcmd(__uint8_t, struct cmdbuf_t *, char);

#ifdef __cplusplus
}
#endif



#endif /* _ARC_COM_IPT_H_ */
