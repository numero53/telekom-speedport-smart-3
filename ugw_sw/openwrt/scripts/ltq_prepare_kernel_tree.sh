#!/usr/bin/env bash
BASEDIR="$PWD"
MK_FILE=include/ltq-prepare-kernel-tree.mk

usage() {
	cat <<EOF
Usage: $0 [option]
	no option, prepares a new kernel tree and assign it in .config
	-o  disables kernel tree in .config
	-e  enables kernel tree in .config
	-u  update target files links
	-d  deletes kernel tree from TOPDIR and disable in .config
	-v  verify uncommitted files and patches
EOF
	exit ${1:-1}
}

tree_make() {
	if [ -f $MK_FILE ]; then
		make -f $MK_FILE $1
	else
		echo "ERROR: File $MK_FILE not found!!"
		exit 1
	fi
}

OPTS="$1"; shift
# for backward compatibility: no option creates new tree
[ -z "$OPTS" ] && OPTS=new

case "$OPTS" in
	-h|help) usage 0;;
	-n|new) tree_make new "$@";;
	-v|verify) tree_make verify "$@";;
	-o|disable) tree_make disable "$@";;
	-e|enable) tree_make enable "$@";;
	-d|delete) tree_make delete "$@";;
	-u|update) tree_make update ;;
	*) tree_make "$@";;
esac
