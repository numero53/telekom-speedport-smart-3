SUBTARGET:=easy85600
BOARDNAME:=EASY85600 (Avinax)
FEATURES:=jffs2 tgz usb
DEVICE_TYPE:=other

DEFAULT_PACKAGES+=ifxos kmod-ifxos 

define Target/Description
	Build for EASY85600 Evaluation Board (TQM8315 based)
endef

