#!/bin/bash

# This script is called during SDK installation.
# Its purpose is to extract Builtdoot, Downloads and Toolchain tarballs.

echo Installing Lantiq OpenWrt SDK...

shopt -s dotglob

if [ -e ./OpenWrt-Buildroot-*.tar.bz2 ]; then
   name=`echo ./OpenWrt-Buildroot-*.tar.bz2 | sed "s@./@@; s@.tar.bz2@@"`

   echo "Extracting Buildroot ($name)..."

   tar xf ./$name.tar.bz2 $name
   cp -a ./$name/* ./
   rm -rf ./$name
fi

if [ -e ./OpenWrt-Downloads-*.tar.bz2 ]; then
   name=`echo ./OpenWrt-Downloads-*.tar.bz2 | sed "s@./@@; s@.tar.bz2@@"`

   echo "Extracting Downloads ($name)..."

   tar xf $name.tar.bz2
   mv ./$name/* ./
   rm -rf ./$name
fi

if [ -e ./OpenWrt-Toolchain-*.tar.bz2 ]; then
   name=`echo ./OpenWrt-Toolchain-*.tar.bz2 | sed "s@./@@; s@.tar.bz2@@"`

   echo "Extracting Toolchain ($name)..."

   mkdir staging_dir

   tar xf $name.tar.bz2
   mv ./$name/* ./staging_dir
   rm -rf ./$name

   toolchain=`echo ./staging_dir/toolchain-*`

   mkdir -p $toolchain/stamp
   touch $toolchain/stamp/.toolchain_install
fi
