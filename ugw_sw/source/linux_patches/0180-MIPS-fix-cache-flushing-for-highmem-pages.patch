From a8d8d6e7d216b93a86856fd2cbf37cf0eb1ae7f2 Mon Sep 17 00:00:00 2001
From: Felix Fietkau <nbd@nbd.name>
Date: Mon, 30 Oct 2017 15:54:54 +0530
Subject: [PATCH] MIPS: fix cache flushing for highmem pages

Most cache flush ops were no-op for highmem pages. This led to nasty
segfaults and (in the case of page_address(page) == NULL) kernel
crashes.

Fix this by always flushing highmem pages using kmap/kunmap_atomic
around the actual cache flush. This might be a bit inefficient, but at
least it's stable.

Signed-off-by: Felix Fietkau <nbd@nbd.name>
---
 arch/mips/mm/cache.c | 7 +++++++
 1 file changed, 7 insertions(+)

diff --git a/arch/mips/mm/cache.c b/arch/mips/mm/cache.c
index 7478e59e786b..d485c3daa7a1 100644
--- a/arch/mips/mm/cache.c
+++ b/arch/mips/mm/cache.c
@@ -117,6 +117,13 @@ void __flush_anon_page(struct page *page, unsigned long vmaddr)
 {
 	unsigned long addr = (unsigned long) page_address(page);
 
+	if (PageHighMem(page)) {
+		addr = (unsigned long)kmap_atomic(page);
+		flush_data_cache_page(addr);
+		__kunmap_atomic((void *)addr);
+		return;
+	}
+
 	if (pages_do_alias(addr, vmaddr)) {
 		if (page_mapcount(page) && !Page_dcache_dirty(page)) {
 			void *kaddr;
