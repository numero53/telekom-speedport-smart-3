From 5140f9d879d241d4ede0209c5b4e9bc1013a4edd Mon Sep 17 00:00:00 2001
From: Felix Fietkau <nbd@nbd.name>
Date: Mon, 30 Oct 2017 15:56:00 +0530
Subject: [PATCH] jffs2: add RENAME_EXCHANGE support

Signed-off-by: Felix Fietkau <nbd@nbd.name>
---
 fs/jffs2/dir.c | 26 ++++++++++++++++++++++----
 1 file changed, 22 insertions(+), 4 deletions(-)

diff --git a/fs/jffs2/dir.c b/fs/jffs2/dir.c
index 0329b6837972..e470891adb1d 100644
--- a/fs/jffs2/dir.c
+++ b/fs/jffs2/dir.c
@@ -777,18 +777,30 @@ static int jffs2_rename (struct inode *old_dir_i, struct dentry *old_dentry,
 	int ret;
 	struct jffs2_sb_info *c = JFFS2_SB_INFO(old_dir_i->i_sb);
 	struct jffs2_inode_info *victim_f = NULL;
+	struct inode *fst_inode = d_inode(old_dentry);
+	struct inode *snd_inode = d_inode(new_dentry);
 	uint8_t type;
 	uint32_t now;
 
-	if (flags & ~(RENAME_NOREPLACE|RENAME_WHITEOUT))
+	if (flags & ~(RENAME_NOREPLACE|RENAME_WHITEOUT|RENAME_EXCHANGE))
 		return -EINVAL;
 
+	if ((flags & RENAME_EXCHANGE) && (old_dir_i != new_dir_i)) {
+		if (S_ISDIR(fst_inode->i_mode) && !S_ISDIR(snd_inode->i_mode)) {
+			inc_nlink(new_dir_i);
+			drop_nlink(old_dir_i);
+		} else if (!S_ISDIR(fst_inode->i_mode) && S_ISDIR(snd_inode->i_mode)) {
+			drop_nlink(new_dir_i);
+			inc_nlink(old_dir_i);
+		}
+	}
+
 	/* The VFS will check for us and prevent trying to rename a
 	 * file over a directory and vice versa, but if it's a directory,
 	 * the VFS can't check whether the victim is empty. The filesystem
 	 * needs to do that for itself.
 	 */
-	if (d_really_is_positive(new_dentry)) {
+	if (d_really_is_positive(new_dentry) && !(flags & RENAME_EXCHANGE)) {
 		victim_f = JFFS2_INODE_INFO(d_inode(new_dentry));
 		if (d_is_dir(new_dentry)) {
 			struct jffs2_full_dirent *fd;
@@ -823,7 +835,7 @@ static int jffs2_rename (struct inode *old_dir_i, struct dentry *old_dentry,
 	if (ret)
 		return ret;
 
-	if (victim_f) {
+	if (victim_f && !(flags & RENAME_EXCHANGE)) {
 		/* There was a victim. Kill it off nicely */
 		if (d_is_dir(new_dentry))
 			clear_nlink(d_inode(new_dentry));
@@ -849,6 +861,12 @@ static int jffs2_rename (struct inode *old_dir_i, struct dentry *old_dentry,
 	if (flags & RENAME_WHITEOUT)
 		/* Replace with whiteout */
 		ret = jffs2_whiteout(old_dir_i, old_dentry);
+	else if (flags & RENAME_EXCHANGE)
+		/* Replace the original */
+		ret = jffs2_do_link(c, JFFS2_INODE_INFO(old_dir_i),
+				    d_inode(new_dentry)->i_ino, type,
+				    old_dentry->d_name.name, old_dentry->d_name.len,
+				    now);
 	else
 		/* Unlink the original */
 		ret = jffs2_do_unlink(c, JFFS2_INODE_INFO(old_dir_i),
@@ -880,7 +898,7 @@ static int jffs2_rename (struct inode *old_dir_i, struct dentry *old_dentry,
 		return ret;
 	}
 
-	if (d_is_dir(old_dentry))
+	if (d_is_dir(old_dentry) && !(flags & RENAME_EXCHANGE))
 		drop_nlink(old_dir_i);
 
 	new_dir_i->i_mtime = new_dir_i->i_ctime = old_dir_i->i_mtime = old_dir_i->i_ctime = ITIME(now);
