From 15a88d69bb192be28bd955649b11b58b50489eb9 Mon Sep 17 00:00:00 2001
From: Daniel Golle <daniel@makrotopia.org>
Date: Mon, 30 Oct 2017 16:15:27 +0530
Subject: [PATCH] ubi: auto-attach mtd device named "ubi" or "data" on boot

Signed-off-by: Daniel Golle <daniel@makrotopia.org>
---
 drivers/mtd/ubi/build.c | 49 +++++++++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 49 insertions(+)

diff --git a/drivers/mtd/ubi/build.c b/drivers/mtd/ubi/build.c
index 6cb5ca52cb5a..417206dbcbb9 100644
--- a/drivers/mtd/ubi/build.c
+++ b/drivers/mtd/ubi/build.c
@@ -1223,6 +1223,49 @@ static struct mtd_info * __init open_mtd_device(const char *mtd_dev)
 	return mtd;
 }
 
+/*
+ * This function tries attaching mtd partitions named either "ubi" or "data"
+ * during boot.
+ */
+static void __init ubi_auto_attach(void)
+{
+	int err;
+	struct mtd_info *mtd;
+
+	/* try attaching mtd device named "ubi" or "data" */
+	mtd = open_mtd_device("ubi");
+	if (IS_ERR(mtd))
+		mtd = open_mtd_device("data");
+
+	if (!IS_ERR(mtd)) {
+		size_t len;
+		char magic[4];
+
+		/* check for a valid ubi magic */
+		err = mtd_read(mtd, 0, 4, &len, (void *) magic);
+		if (!err && len == 4 && strncmp(magic, "UBI#", 4)) {
+			pr_err("UBI error: no valid UBI magic found inside mtd%d", mtd->index);
+			put_mtd_device(mtd);
+			return;
+		}
+
+		/* auto-add only media types where UBI makes sense */
+		if (mtd->type == MTD_NANDFLASH ||
+		    mtd->type == MTD_NORFLASH ||
+		    mtd->type == MTD_DATAFLASH ||
+		    mtd->type == MTD_MLCNANDFLASH) {
+			mutex_lock(&ubi_devices_mutex);
+			pr_notice("UBI: auto-attach mtd%d\n", mtd->index);
+			err = ubi_attach_mtd_dev(mtd, UBI_DEV_NUM_AUTO, 0, 0);
+			mutex_unlock(&ubi_devices_mutex);
+			if (err < 0) {
+				pr_err("UBI error: cannot attach mtd%d", mtd->index);
+				put_mtd_device(mtd);
+			}
+		}
+	}
+}
+
 static int __init ubi_init(void)
 {
 	int err, i, k;
@@ -1306,6 +1349,12 @@ static int __init ubi_init(void)
 		}
 	}
 
+	/* auto-attach mtd devices only if built-in to the kernel and no ubi.mtd
+	 * parameter was given */
+	if (IS_ENABLED(CONFIG_MTD_ROOTFS_ROOT_DEV) &&
+	    !ubi_is_module() && !mtd_devs)
+		ubi_auto_attach();
+
 	err = ubiblock_init();
 	if (err) {
 		pr_err("UBI error: block: cannot initialize, error %d", err);
