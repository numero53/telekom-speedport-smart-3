#!/bin/bash
#
# UGW Clone prepare script
# Copyright (C) 2017 Intel Corporation
#

# definition of LEDE base version (17.01)
OWRT_GIT_URL="https://git.openwrt.org/openwrt/openwrt.git"
OWRT_GIT_BRANCH="lede-17.01"
OWRT_GIT_REV="tags/v17.01.4"
OWRT_CLONE_DIR="openwrt"

OWRT_PACKAGES_GIT_URL="https://github.com/openwrt/packages.git"
OWRT_PACKAGES_GIT_BRANCH="lede-17.01"
OWRT_PACKAGES_GIT_REV=""
OWRT_PACKAGES_CLONE_DIR="feeds/packages"

OWRT_LUCI_GIT_URL="https://git.lede-project.org/project/luci.git"
OWRT_LUCI_GIT_BRANCH="lede-17.01"
OWRT_LUCI_GIT_REV=""
OWRT_LUCI_CLONE_DIR="feeds/luci"

TARGET_FEEDS="feeds/ugw/targets"
OVERRIDE_FEEDS="opensource"

LINUX_URL="https://cdn.kernel.org/pub/linux/kernel/v4.x"

kernel_repos="linux linux_750"
linux_url="$LINUX_URL"
linux_750_url="$LINUX_URL"

clone_linux_patch_dirs="patches/linux"

c_clr="\033[00;00m"
c_red="\033[31;01m"
c_gre="\033[32;01m"
c_yel="\033[33;01m"
c_blu="\033[34;01m"
c_mag="\033[35;01m"
c_cya="\033[36;01m"
c_und="\033[00;04m"

_help()
{
	_title "Usage: $0 [options] `[ -z "$_CDSRC" ] && echo [tag]`";
	echo "    If no arguments are passed, then this will clone '$OWRT_CLONE_DIR' repo, apply patches, install all feeds and make it ready for build."
	[ -z "$_CDSRC" ] && echo "    [tag] If a fixed tag is passed as an argument, then it resets the source code in all the repos to a specific tag and prepares as mentioned above."
	echo -en "\n    ${c_und}Options:-${c_clr}\n"
	_hlptxt -o "Clean entire '$OWRT_CLONE_DIR' clone, Removes applied patches and revert back to original source."
	_hlptxt -i "Re-install all feeds."
	_hlptxt -r "Clean and resets to original source code. This also deletes downloaded '$OWRT_CLONE_DIR' and feeds."
	_hlptxt -m "Generate models per variant from multi-image based configuration."
	[ -z "$_CDSRC" ] && _hlptxt -u "Update all repos to proper branch and local tip. This doesnot pull or doesnot clean your private changes."
	[ -z "$_CDSRC" ] && _hlptxt -U "Pull and update all repos to proper branch and remote tip. This will also remove your private changes."
	[ -z "$_CDSRC" ] && _hlptxt -t [1/0] "Create a composite tag. Pass 1 for 'built' tag and 0 for 'rejected' tag (This option is only for buildbot)."
	[ -z "$_CDSRC" ] && _hlptxt -g "Generate fixed tag manifest file '$manifestfilename'"
	[ -z "$_CDSRC" ] && _hlptxt -l "Generate Changelog file '$repochangelog' of all repos between fixed tag and head."
	[ -z "$_CDSRC" -a "$kernel_repos" ] && _hlptxt -k "Clean linux source repos."
	[ -z "$_CDSRC" -a "$kernel_repos" ] && _hlptxt -s "Sync linux source repos to its cloned mirror repos."
	_hlptxt -f "Do with force. No confirmation asked. Use with other options."
	_hlptxt -h "Show this help."
	[ -n "$1" ] && {
		exit $1;
	}
}

_title()
{
	echo -en "\n${c_blu}$@${c_clr}\n";
}

_info()
{
	echo -en "\n${c_gre}$@${c_clr}\n";
}

_err()
{
	echo -en "${c_red}Error: $@${c_clr}\n";
}

_warn()
{
	echo -en "${c_mag}Warning: $@${c_clr}\n";
}

_die()
{
	_err "$@";
	popd > /dev/null
	exit 1;
}

_hlptxt()
{
	local _opt="$1"
	shift
	echo -en "      $c_red${_opt}${c_clr}  $@\n"
}

check_git()
{
	which git > /dev/null || _die "Missing 'git' tool. Please install 'git' in your PC to continue."
	git config --get user.name > /dev/null || _die "Specify your user name with: git config --global user.name \"Firstname Lastname\""
	git config --get user.email > /dev/null || _die "specify your user email with: git config --global user.email \"firstname.lastname@domain.com\""
}

#
# Usage: opt_arg_process <options> $@
# Example: opt_arg_process "abcefij" $@  # Here options are: a, b, c, e, f, i, j.
#    If options needs to take additional argument, then specify a notation (') after the option.
# Example: opt_arg_process "ab'cef'ij" $@   # Here b and f needs additional argument.
#
# Result when used in script:-
# Variable named "$opt_args" may contain one or more values (separated with spaces) which was not passed under any option.
#	Example:- ./script_name.sh hello test
#		opt_args="hello test"
# <option>_option will be set with 1 and <option>_opt_args may have single value which was passed under this option.
#	Example:- ./script_name.sh -t abc -n hello
#               t_option=1
#		t_opt_args="abc"
#               n_option=1
#		n_opt_args="hello"
#
#       Example:- ./script_name.sh test -t abc -n hello welcome -i xyz world -f
#		opt_args="test welcome world"
#               t_option=1
#		t_opt_args="abc"
#               n_option=1
#		n_opt_args="hello"
#               i_option=1
#		i_opt_args="xyz"
#		f_option=1
#		f_opt_args=""
#
opt_arg_process()
{
	local options opt _tA _tB _tArg;
	[ -n "$1" ] || return;

	options="${1// }"
	if [ "${options::1}" = "'" ]; then
		_die "Fix options passed to opt_arg_process function! Arg notation (') should be passed after the option. Example:- \"a' b' c'\" etc."
	fi
	shift;
	[ $# = 0 ] && no_option=1;
	while [ $# != 0 ]; do
		if [ "${1::1}" = "-" ]; then
			opt=${1//-};
			opt=${opt::1};
			_tB="$options"
			while [ -n "$_tB" ]; do
				_tA=${_tB::1};
				_tB=${_tB:1};
				_tArg=${_tB::1};
				[ -n "$_tArg" -a "$_tArg" = "'" ] && {
					_tB=${_tB:1};
				} || _tArg=""
				if [ "$_tA" = "$opt" ]; then
					eval ${opt}_option=1;
					[ -z "$_tArg" ] && {
						opt="";
					}
					break;
				fi
				_tA="";_tArg="";
			done
			[ -z "$_tA" ] && {
				_err "Invalid option $1";
				_help 1;
			}
		else
			[ -n "$opt" ] && {
				eval ${opt}_opt_args="$1";
				opt="";
			} || {
				opt_args="$opt_args $1"
			}
		fi
		shift
	done
}

# arguments: 1: repo path, 2: revision
update_shallow_clone_to_rev()
{
	local repopath="$1"
	local revision="$2"
	local branch="$3"
	local depth="0"
	local incr="100"
	local limit="300"

	if [ -d "$repopath" -a -n "$revision" ]; then
		_info "Updating $repopath to revision $revision"
		cd "$repopath"/ && {
			git checkout "$branch" || _die "Unable to checkout to branch: $branch"
			while [ ! "$(git reset -q --hard $revision 2>/dev/null ; echo $?)" -eq 0 ]; do
				depth=$[$depth+$incr]
				git fetch --depth $depth --update-shallow
				[ $depth -ge $[$limit+$incr] ] && error_print "Unable to update $repopath to a specified revision."
				if [ $depth -ge $limit ]; then
					_info "Fetching full repo!"
					git fetch --unshallow --update-shallow
				fi
			done
			cd - >/dev/null
		}
	fi
}

#arguments: 1: repo path to clone, 2: url, 3: branch, 4: revision, 5: patch path
clone_and_patch()
{
	local repopath="$1"
	local url="$2"
	local branch="$3"
	local revision="$4"
	local patchpath="$5"

	[ -n "$repopath" -a -n "$url" -a -n "$branch" -a -n "$repopath" ] || _die "clone_openwrt: Missing proper arguments <url> <branch> <path to clone> <revision>"

	[ -d "$repopath" ] || {

		_title "Cloning '$repopath'.."

		git clone --depth 100 --single-branch --branch "$branch" "$url" "$repopath" || \
			_die "Unable to download from url '$url'. Please check Internet access or your proxy settings."

		update_shallow_clone_to_rev "$repopath" "$revision" "$branch"
	}
	git_apply_patches "$repopath" "$patchpath"
}

# arguments: 1: list of active guards for a patch, 2: list of selected guards
match_guards()
{
	local result=1
	local g1 g2 sel res
	for g1 in $1; do
		case "$g1" in
		"#+"*)
			res=0
			g2=${g1###+}
			for sel in $2; do
				[ "$g2" = "$sel" ] && res=1
			done
			result=`expr $result \* $res`
			;;
		"#-"*)
			res=1
			g2=${g1###-}
			for sel in $2; do
				[ "$g2" = "$sel" ] && res=0
			done
			result=`expr $result \* $res`
			;;
		esac
	done
	return `expr 1 - $result`
}

# create clean list of patches, depending on selected guards
# arguments: 1: path to series file, 2+: list of selected guards
filter_patches()
{
	local seriesfile=$1
	shift
	local guard_sel=$*
	local patch
	local guards

	while read patch guards; do
		if match_guards "${guards}" "${guard_sel}"; then
			echo "$patch"
		fi
	done < ${seriesfile}
}

# arguments: 1: repo path to apply patches, 2: path to patchdir, [3+: selected guards]
git_apply_patches()
{
	local repopath="$1"
	local patchdir="$2"
	local author="`git config --get user.name` <`git config --get user.email`>"
	local series="series"
	shift
	local guards=$*
	local tmpbranch

	[ -d "$repopath" ] || _die "Unable to find clone path $repopath"

	cd "$patchdir" && {
		[ -f "$series" ] || _die "Unable to find series file $patchdir/$series"
		patchdir=$(pwd);
		cd - >/dev/null
	} || return;

	cd "$repopath" && {
		tmpbranch="$(git branch|grep "\*"|awk '{ print $2 }')";
		if [ "$tmpbranch" != "patch" ]; then
			_title "Applying patches to '$repopath'.."

			git checkout -B patch || _die "Preparing patch-branch failed!"
			git clean -ffd

			# we apply patches of patches, which needs some whitespaces in
			# patches itself, so ignore warnings during apply
			git config --local --add apply.whitespace nowarn
			filter_patches ${patchdir}/${series} ${guards}
			git quiltimport --author "${author}" --patches ${patchdir} --series <(filter_patches ${patchdir}/${series} ${guards}) || _die "git quiltimport failed to apply patches"
		fi
		cd - >/dev/null
	}
}

# Find and install all target feeds. Output in '$targetspath' variable.
find_target_feeds()
{
	local rtmp tdir tname;
	targetspath="";
	for tdir in $TARGET_FEEDS; do
		if [ -d "$tdir" ]; then
			for tname in `ls "$tdir"/* -d`; do
				rtmp="$(basename $tname)";
				[ -n "$rtmp" ] && {
					targetspath="$targetspath $rtmp"
				}
			done
		fi
	done
}

#args: 1: repopath
clean_feeds()
{
	local repopath="$1"
	local tdir;
	[ -d "$repopath" ] && {
		find_target_feeds
		cd "$repopath" && {
			rm -rf feeds package/feeds
			cd - >/dev/null
		}
		for tdir in $targetspath; do
			if [ -h $OWRT_CLONE_DIR/target/linux/$tdir ]; then
				rm -f $OWRT_CLONE_DIR/target/linux/$tdir;
				sed -i '/target\/linux\/'$tdir'/d' $OWRT_CLONE_DIR/.gitignore
			fi
		done
	}

	rm -f $OWRT_CLONE_DIR/ugw_version
	if [ -f $OWRT_CLONE_DIR/.gitignore ]; then
		sed -i '/ugw_version/d' $OWRT_CLONE_DIR/.gitignore
	fi
}

# Expand feeds if feeds.conf entry have wildchar '*'.
expand_feeds_conf()
{
	local _thispath="$1"
	local _line _fir _sec _thi _buf _i
	while read _line; do
		_line="${_line//\*/\\*}"
		_buf=( $_line );
		_fir="${_buf[0]}";
		_sec="${_buf[1]}";
		_thi="${_buf[2]}";
		if [ "{_fir::1}" = "#" ]; then
			echo "$_line"
		elif [ -z "${_sec/*\*/}" ]; then
			_sec="${_sec//\\\*/}"
			_thi="${_thi/\.\.\/\.\.\//}"
			if [ -d "$_thispath/$_thi" ]; then
				cd "$_thispath/$_thi"/ && {
					for _i in `ls -d *`; do
						echo "$_fir ${_sec}$_i ../../${_thi}/$_i"
					done
					cd - >/dev/null
				}
			fi
		else
			echo "$_line"
		fi
	done
}

#args: 1: repopath
install_feeds()
{
	local _thispath="$(pwd)"
	local repopath="$1"
	local feeds_config="$_thispath/feeds.conf"
	local tdir;
	[ -d "$repopath" ] || _die "Path '$repopath' doesn't exist.";

	[ -d "$repopath/feeds" -a -d "$repopath/package/feeds" ] || {
		clean_feeds $OWRT_CLONE_DIR;
		find_target_feeds
		cd "$repopath" && {
			[ -n "$2" ] || _title "Installing feeds.."
			expand_feeds_conf "$_thispath" < "$feeds_config" > feeds.conf
			#cp -f "$feeds_config" feeds.conf
			./scripts/feeds update -a
			./scripts/feeds install -a 2>/dev/null

			for tdir in $OVERRIDE_FEEDS; do
				_info "Overriding feeds with packages from '${tdir}'.."
				./scripts/feeds install -a -f -p ${tdir}
			done

			for tdir in $targetspath; do
				_info "Installing target feed '${tdir}'.."
				./scripts/feeds install $tdir
				echo "target/linux/$tdir" >> .gitignore
			done

			rm -f tmp/info/.packageinfo-kernel_linux
			cd - >/dev/null
		}
		cd "$_thispath/"
	}

	if [ -f ugw_version ]; then
		cp -f ugw_version $OWRT_CLONE_DIR/;
		sed -i '/ugw_version/d' $OWRT_CLONE_DIR/.gitignore
		echo ugw_version >> $OWRT_CLONE_DIR/.gitignore
	fi
}

reinstall_feeds()
{
	local _cfg_found;
	_title "Re-installing feeds.."
	if [ -f $OWRT_CLONE_DIR/.config ]; then
		cd $OWRT_CLONE_DIR/ && {
			_cfg_found=1;
			rm -f .config-backup__;
			cp -f .config .config-backup__ 2>/dev/null;
			cd - >/dev/null
		}
	fi
	clean_feeds "$1"
	install_feeds "$1" 1

	if [ -n "$_cfg_found" ]; then
		cd $OWRT_CLONE_DIR/ && {
			mv -f .config-backup__ .config 2>/dev/null;
			cd - >/dev/null
		}
		_info "Feeds re-installed. You might need to run 'make defconfig' or 'make menuconfig' from ${OWRT_CLONE_DIR}/ to reflect new feed changes in your .config"
	else
		_info "Feeds re-installed. Select new model by './scripts/ltq_change_environment.sh switch' from ${OWRT_CLONE_DIR}/ and build."
	fi
}

prepare_linux()
{
	local _lndir _linux_ver _linux_file_ver _linux_tar _linux_url

	for _lndir in $kernel_repos; do
		unset _linux_ver _linux_file_ver _linux_tar _linux_url

		if [ -d "./source/$_lndir/" -o ! -d "./source/${_lndir}_patches/" ]; then
			continue;
		fi

		_linux_file_ver="$(cat "./source/${_lndir}_patches/linux_version" 2>/dev/null)"
		eval _linux_ver='${'${_lndir}_version:-${_linux_file_ver:1}'}'
		eval _linux_url='$'${_lndir}_url;
		_linux_url="${_linux_url:-$LINUX_URL}";

		if [ -z "$_linux_ver" ]; then
			_die "Unable to find linux version for '$_lndir'"
		fi
		if [ -z "$_linux_url" ]; then
			_die "Unable to find linux download url for '$_lndir'"
		fi

		_title "Preparing linux kernel source tree version linux-${_linux_ver} .."

		_linux_tar="linux-${_linux_ver}.tar.xz"
		_linux_url="$_linux_url/$_linux_tar"

		pushd . >/dev/null
		cd source/ && {
			rm -rf linux-${_linux_ver}
			rm -f ${_linux_tar}*
			if [ -f "../dl/$_linux_tar" ]; then
				cp ../dl/$_linux_tar .;
			else 
				wget $_linux_url;
			fi
			if [ $? -eq 0 ]; then
				xzcat $_linux_tar | tar -xvf -
				if [ $? -eq 0 ]; then
					rm -f ${_linux_tar}*
					cd linux-${_linux_ver}/ && {
						git init
						git add -A
						git commit -am "linux-${_linux_ver}"
						git config --local --add apply.whitespace nowarn
						_info "Applying UGW kernel patches.."
						git quiltimport --patches ../${_lndir}_patches/
						cd - >/dev/null
					}
					mv linux-${_linux_ver} ${_lndir};
				else
					popd > /dev/null
					_die "Unable to extract downloaded linux tarball 'linux-${_linux_ver}.tar.xz'. Please check!!"
				fi
			else
				popd > /dev/null
				_die "Unable to download and prepare linux-${_linux_ver} source code. Please check the url: $_linux_url"
			fi
		}
		popd > /dev/null
	done
}

setup_dl()
{
	if [ ! -d "$OWRT_CLONE_DIR/dl/" ] && [ -d dl/ ]; then
		_title "Copying tarballs.."
		cp -rvf dl "$OWRT_CLONE_DIR"/;
	fi
}

patch_linux_clone()
{
	local _patchdir _linuxdir

	# These patches should not be applied while compiling CD"
	if [ -d "source/linux" ] && [ ! -d "source/linux_patches" ]; then
		if [ -n "$clone_linux_patch_dirs" ]; then
			for _patchdir in $clone_linux_patch_dirs; do
				_linuxdir="$(basename $_patchdir)";
				if [ -d "source/$_linuxdir" ]; then
					git_apply_patches "source/$_linuxdir" "$_patchdir"
				fi
			done
		fi
	fi
}

clone_all()
{
	check_git
	clone_and_patch $OWRT_CLONE_DIR "$OWRT_GIT_URL" "$OWRT_GIT_BRANCH" "$OWRT_GIT_REV" patches/core
	clone_and_patch $OWRT_PACKAGES_CLONE_DIR "$OWRT_PACKAGES_GIT_URL" "$OWRT_PACKAGES_GIT_BRANCH" "$OWRT_PACKAGES_GIT_REV" patches/packages
	clone_and_patch $OWRT_LUCI_CLONE_DIR "$OWRT_LUCI_GIT_URL" "$OWRT_LUCI_GIT_BRANCH" "$OWRT_LUCI_GIT_REV" patches/luci
	patch_linux_clone
}

confirm_action()
{
	local inp;
	[ -n "$O_FORCE" ] || {
		_warn "$1"
		echo -en "Do you want to continue..? (y/N) "
		read inp;
		if [ -n "$inp" ] && [ "$inp" = "y" -o "$inp" = "Y" ]; then
			return;
		fi
		exit 0;
	}
}

clean_linux()
{
	local ldir lrepo t_found

	if [ -d source/ ]; then
		for lrepo in $kernel_repos; do
			if [ -d source/$lrepo ]; then
				cd source/$lrepo/ && {
					git clean -fdx;
					cd - >/dev/null;
				}
			fi
		done

		cd source/ && {
			for ldir in linux*; do
				t_found=""
				for lrepo in $kernel_repos; do
					if [ "$ldir" = "$lrepo" -o "$ldir" = "${lrepo}_patches" ]; then
						t_found=1
						break;
					fi
				done
				[ -z "$t_found" ] && {
					echo "Removing $ldir";
					rm -rf "$ldir";
				}
			done
			cd - >/dev/null;
		}
	fi
}

clean_linux_clone()
{
	local _patchdir _linuxdir

	# These patches should not be applied while compiling CD"
	if [ -d "source/linux" ] && [ ! -d "source/linux_patches" ]; then
		if [ -n "$clone_linux_patch_dirs" ]; then
			for _patchdir in $clone_linux_patch_dirs; do
				_linuxdir="$(basename $_patchdir)";
				if [ -d "source/$_linuxdir" ]; then
					clean_clone "source/$_linuxdir"
				fi
			done
		fi
	fi
}

clean_all()
{
	check_git
	confirm_action "This will remove all private, un-checked files and build directories from '$OWRT_CLONE_DIR', '$OWRT_PACKAGES_CLONE_DIR' and linux source path. Also removes buildsystem patches".
	clean_clone $OWRT_CLONE_DIR "$OWRT_GIT_BRANCH" "$OWRT_GIT_REV"
	clean_clone $OWRT_PACKAGES_CLONE_DIR "$OWRT_PACKAGES_GIT_BRANCH" "$OWRT_PACKAGES_GIT_REV"
	clean_clone $OWRT_LUCI_CLONE_DIR "$OWRT_LUCI_GIT_BRANCH" "$OWRT_LUCI_GIT_REV"
	clean_linux_clone
	clean_linux
}

sync_linux_mirror()
{
	local _dir _remote _branch _commitid
	cd source/ && {
		_title "Syncing linux repo mirrors.."
		for _dir in $(ls * -d); do
			_remote=""
			pushd . >/dev/null
			cd $_dir/ && {
				_remote="$(git remote -v 2>/dev/null|head -1|awk '{ print $2 }')"
				if [ -n "$_remote" -a -d "$_remote" ]; then
					_info "Syncing 'source/$_dir' with '$_remote'"
					git reset --hard -q
					git fetch --all
					git fetch --tags
					git reset --hard -q
					git pull
					_commitid=""
					_branch=""
					pushd . >/dev/null
					cd "$_remote"/ && {
						_commitid="$(git log -1 --pretty=format:"%H")";
						_branch="$(git branch --contains "$_commitid" 2>/dev/null|grep "\* "|awk '{ print $2 }')"
						if [ -n "$_branch" ]; then
							if [ "${_branch::1}" = "(" ]; then
								_branch="";
							fi
						fi
					}
					popd >/dev/null
					if [ -n "$_branch" -a -n "$_commitid" ]; then
						git checkout "$_branch"
						git reset --hard "$_commitid"
					elif [ -n "$_commitid" ]; then
						git checkout "$_commitid"
					fi
					rm -f .config*
					rm -rf user_headers
				fi
			}
			popd >/dev/null
		done
		cd - >/dev/null
	}
}

reset_all()
{

	confirm_action "This will delete '$OWRT_CLONE_DIR', '$OWRT_PACKAGES_CLONE_DIR' '$OWRT_LUCI_CLONE_DIR' and downloaded linux source directory if any. All build directories and private files under this will be lost."
	O_FORCE=1 clean_all;

	local ldir lrepo t_found
	rm -rf $OWRT_CLONE_DIR
	rm -rf $OWRT_PACKAGES_CLONE_DIR
	rm -rf $OWRT_LUCI_CLONE_DIR
	cd source/ && {
		for ldir in linux*; do
			t_found=""
			for lrepo in $kernel_repos; do
				# Below will work only for CDs.
				if [ "$ldir" = "${lrepo}_patches" ]; then
					t_found=1
					break;
				fi
				# Delete linux repos for CD as it will be a local clone
				if [ -z $_CDSRC -a "$ldir" = "$lrepo" ]; then
					t_found=1
					break;
				fi
			done
			[ -z "$t_found" ] && {
				echo "Removing $ldir";
				rm -rf "$ldir";
			}
		done
		cd - >/dev/null;
	}
}

clean_clone()
{
	local repopath="$1"
	local branch="$2"
	local revision="$3"
	local tmpbranch;

	if [ -d "$repopath" ]; then
		cd "$repopath"/ && {
			_title "Cleaning '$repopath'.."
			git clean -ffdx
			if [ -z "$branch" ]; then
				tmpbranch="$(git branch|grep "\*"|awk '{ print $2 }')";
				if [ "$tmpbranch" != "patch" ]; then
					return
				fi
				branch="$(git branch 2>/dev/null|grep -B1 "^\* "|grep -v "^\* "|awk '{ print $1 }')"
			fi
			git checkout "$branch"
			git branch -D patch 2>/dev/null
			git reset --hard
			cd - >/dev/null;
		}
		update_shallow_clone_to_rev "$repopath" "$revision" "$branch"
	fi
}

models_per_variant()
{
	local cfg active_model target_devices bin_path cfg_name
	_title "Generating models per variant from current model.."

	cd $OWRT_CLONE_DIR/ && {
		if ! [ -f ./active_config -a -f ./.config ]; then
			_die "Please select a model and proceed..!".
		fi

		active_model="$(cat ./active_config)"
		if ! [ -f "${active_model}/.config" ]; then
			_die "Please select a valid model and proceed..!".
		fi

		target_devices="$(grep CONFIG_TARGET_DEVICE_.*=y .config|cut -d= -f1)";
		if [ -z "$target_devices" ]; then
			_die "Selected model does not support TARGET_DEVICES..!";
		fi

		_info "Target Devices found:-"
		echo -en "$target_devices\n"

		echo -en "TOPDIR=\${CURDIR}\ninclude rules.mk\nall:\n\t@echo \$(BIN_DIR)\n" > /tmp/${USER}_test.mk
		bin_path="$(make -f /tmp/${USER}_test.mk 2>/dev/null)"
		rm -f /tmp/${USER}_test.mk
		[ -z "$bin_path" ] && bin_path="."

		rm -f __config_old;
		cp -f .config __config_old;
		for cfg in $target_devices; do
			_info "Generating config: ${cfg//CONFIG_TARGET_DEVICE_}.config"
			rm -f .config;
			cp ${active_model}/.config .;
			sed -i '/CONFIG_TARGET_DEVICE_.*=y/d' .config;
			echo $cfg=y >> .config;
			make defconfig;
			rm -f $bin_path/${cfg//CONFIG_TARGET_DEVICE_}.config;
			mv -f .config $bin_path/${cfg//CONFIG_TARGET_DEVICE_}.config;
		done
		mv -f __config_old .config
		echo "Config files generated under $bin_path/"
		cd - >/dev/null
	}
}

environment_check()
{
	# This script name and path
	THIS_SCRIPT="`basename $0`";
	THIS_SCRIPT_PATH="$(cd `dirname $0` && pwd -P)";\

	cd $THIS_SCRIPT_PATH/
	pushd . >/dev/null

	[ -f "ugw_version" -a -d "feeds" -a -d "config" ] || {
		_die "Run this script under ugw_sw/ clone!!"
	}

	[ -d .git ] || {
		_CDSRC=1;
	}
}

## UGW_SW_CLONE_OPERATIONS ###
default_manifestfile="../.repo/manifests/default.xml"
manifestfilename=".manifest-tag.xml"
repochangelog="repo_changelog.txt"
changelog_excludes="ugw_sw"

use_fixed_tag_manifest()
{
	if [ -f "$manifestfilename" ]; then
		cp -f "$manifestfilename" ../.repo/manifests/default.xml
	else
		if [ -n "$1" ]; then
			_die "Fixed tag manifest file '$manifestfilename' not found! Unable to update to a fixed tag."
		fi
	fi
}

revert_fixed_tag_manifest()
{
	cd ../.repo/manifests/ && {
		git checkout default.xml
		cd - >/dev/null
	}
}

update_clone()
{
	local _command;
	[ -n "$1" ] && {
		confirm_action "This command will pull and update all repos to tip and also removes uncommited and private files from all repos."
	}

	if [ -n "$1" ]; then
		repo forall -c 'git reset --hard; git clean -fdx'
		repo sync --force-sync --prune -d
		repo forall -c 'git clean -fdx; git fetch --all; git checkout $REPO_RREV; git reset --hard remotes/$REPO_REMOTE/$REPO_RREV';
	else
		repo forall -c 'git checkout $REPO_RREV';
	fi

	if [ -d "$OWRT_CLONE_DIR" -o -d "$OWRT_PACKAGES_CLONE_DIR" -o -d "$OWRT_LUCI_CLONE_DIR" ]; then
		_warn "You might need to update '$OWRT_CLONE_DIR' '$OWRT_PACKAGES_CLONE_DIR' and '$OWRT_LUCI_CLONE_DIR' after this update. Please clean update by '$0 -o' and '$0'"
	fi
}

update_clone_manifest()
{
	local tagname="$1";
	_title "Updating repo to fixed tag: '$tagname'..";
	repo sync --force-sync --prune -d
	repo forall -c '_path="`basename $(pwd)`"; if [ "$_path" = "$REPO_PATH" ]; then \
		git clean -fdxq; git reset --hard -q; git checkout $REPO_RREV; git fetch --all; git fetch --tags; git reset --hard remotes/$REPO_REMOTE/$REPO_RREV; \
	fi'
	git reset --hard "$tagname" || _die "Unable to reset the clone to a fixed tag: '$1'! Please provide a valid tag."
	use_fixed_tag_manifest 1
	export tagname;
	repo forall -v -c '_path="`basename $(pwd)`"; if [ "$_path" = "$REPO_PATH" ]; then \
		git clean -fdxq; git checkout $REPO_RREV; git reset --hard $tagname; \
	else \
		git clean -fdx; git checkout $REPO_RREV; git reset --hard $REPO__hashtag; \
	fi'
	revert_fixed_tag_manifest

	if [ -d "$OWRT_CLONE_DIR" -o -d "$OWRT_PACKAGES_CLONE_DIR" -o -d "$OWRT_LUCI_CLONE_DIR" ]; then
		_warn "You might need to update '$OWRT_CLONE_DIR' '$OWRT_PACKAGES_CLONE_DIR' and '$OWRT_LUCI_CLONE_DIR' after this update. Please clean update by '$0 -o' and '$0'"
		exit 0;
	fi
}

generate_tag_manifest()
{
	local dflt_manifest_repo_hash;

	_title "Generating fixed tag manifest file '$manifestfilename'"

	repo manifest -r >/dev/null 2>/dev/null || {
		_die "Unable to generate fixed tag manifest file. Please do 'repo sync' and try again."
	}

	rm -f "$manifestfilename"
	repo manifest -r|grep -q "<annotation" && {
		repo manifest -r | sed -e '/<annotation/d' -e '/<\/project>/d' -e 's/revision=\(.*\) upstream=\(.*\)>/revision=\2>\n\t<annotation name="hashtag" value=\1\/>\n  <\/project>/g' > "$manifestfilename"
		true
	} || {
		repo manifest -r|sed 's/revision=\(.*\) upstream=\(.*\)\/>/revision=\2>\n\t<annotation name="hashtag" value=\1\/>\n  <\/project>/g' > "$manifestfilename"
	}

	[ -f "$manifestfilename" ] && {
		if [ -f "$default_manifestfile" ]; then
			sed 's/path=\(.*\) revision=\(.*\)/path=\1 remote="sw_ugw" revision=\2/' -i "$manifestfilename"
			sed 's/remote="sw_pon" remote="sw_ugw"/remote="sw_pon"/' -i "$manifestfilename"
			cd `dirname "$default_manifestfile"`/ && {
				dflt_manifest_repo_hash="$(git rev-parse HEAD)"
				cd - >/dev/null
			}
			sed 's/\(<default.*\/>\)/\1\n\n  <manifesthash value="'$dflt_manifest_repo_hash'"\/>/' -i "$manifestfilename"
		fi
		_info "Generated file '$manifestfilename'"
	}
}

# Generates fixed and floating tags, commit and push.
# Pass 1 (success) or 0 (failure). 2nd and 3rd arg can be optional fixed and floating tag passed externally.
generate_composite_tag()
{
	local _ii;
	local _TAG="$(git rev-parse --abbrev-ref HEAD)"
	local _RREMOTE="$(git remote)"
	_TAG="${_TAG/heads\//}"
	local _FIXEDTAG="${_TAG}_$(date +%Y%m%dT%H%M)"
	local _comment;
	if [ -n "$1" -a "$1" = "1" ]; then
		_TAG="built_${_TAG}";
		_FIXEDTAG="built_${_FIXEDTAG}";
		_comment="Built successfully"
	elif [ -n "$1" -a "$1" = "0" ]; then
		_TAG="rejected_${_TAG}";
		_FIXEDTAG="rejected_${_FIXEDTAG}";
		_comment="Build failure"
	else
		_die "Pass 1 (built) or 0 (rejected) as an option"
	fi

	if [ -n "$2" ]; then
		_FIXEDTAG="";
		_TAG="";
		for _ii in $2; do
			[ -z "$_FIXEDTAG" ] && _FIXEDTAG="$_ii" || {
				[ -z "$_TAG" ] && _TAG="$_ii";
			}
		done
	fi

	generate_tag_manifest
	git pull
	git add "$manifestfilename"
	git commit -m "${_comment} with UGW tag: ${_FIXEDTAG}" "$manifestfilename"
	git tag $_FIXEDTAG

	#confirm_action "This will push the fixed tag manifest file to remote and tags fixed and floating tag."
	git push || _err "Push failed! Unable to push fixed tag manifest file commit for '${_FIXEDTAG}'!"
	git tag -d $_TAG
	git push $_RREMOTE :refs/tags/${_TAG} || _err "Push failed! Deletion of floating tag '${_TAG}' failed in remote."
	git tag $_TAG
	git push $_RREMOTE $_FIXEDTAG || _err "Push failed! Failed to push fixed tag '${_FIXEDTAG}' in remote!"
	git push $_RREMOTE $_TAG || _err "Push failed! Failed to push floating tag '${_TAG}' in remote!"
	exit 0;
}

generate_changelog()
{
	local _thispath="$(pwd)"
	local _manifestpath="$_thispath/$manifestfilename"
	local _changelogfile="$_thispath/$repochangelog"
	local _dfltmanifest="$_thispath/$default_manifestfile"
	local _gitlog="git log --pretty=format:\"commit: %h - %cd %d [%an <%ae>]%nmessage: %s%n\""

	get_commit_from_manifest()
	{
		local _path=`echo $4|sed 's|\/|\\\/|g'`
		sed -n '/name="'$2'".*path="'$_path'".*remote="'$3'"/,/<\/project>/{n;s/.*value="\(.*\)"\/>/\1/p}' "$1"
	}

	add_line_command()
	{
		local _cnt _i _line;
		_cnt=`echo "$2"|wc -c`;
		for ((_i=0; _i<$_cnt; _i++)); do
			_line="-$_line";
		done;
		echo -en "$_line\n$1\n$2\n$_line\n";
	}

	print_gitlog()
	{
		local REPO_PROJECT="$1"
		local REPO_REMOTE="$2"
		local _line _flg;

		while read _line; do
			[ -z "$_flg" ] && {
				add_line_command "REPO NAME: $REPO_PROJECT" "REPO URL: `git remote get-url $REPO_REMOTE`";
				_flg=1
			}
			echo "$_line";
		done
		[ -n "$_flg" ] && {
			echo;
		}
	}

	export -f get_commit_from_manifest add_line_command print_gitlog
	export _gitlog _manifestpath changelog_excludes;

	generate_clog() {
		local _manifest_rev;
		if [ -f "$_manifestpath" ]; then
			if [ -f "$_dfltmanifest" ]; then
				_manifest_rev="$(sed -n 's/.*<manifesthash.*value="\(.*\)"\/>/\1/p' "$_manifestpath")"
				cd `dirname "$_dfltmanifest"` && {
					eval $_gitlog ${_manifest_rev}..$(git rev-parse HEAD) | print_gitlog manifest origin
					cd - >/dev/null;
				}
			fi
			repo forall -c ' \
				for _ii in $changelog_excludes; do \
					[ "$REPO_PROJECT" = "$_ii" ] && exit; \
				done; \
				eval $_gitlog `get_commit_from_manifest $_manifestpath $REPO_PROJECT $REPO_REMOTE $REPO_PATH`..$REPO_LREV | print_gitlog "$REPO_PROJECT" "$REPO_REMOTE"
			'
		else
			_warn "Unable to find fixed tag manifest file '$_manifestpath' to do this operation properly!!"
		fi
	}

	rm -f "$_changelogfile"

	generate_clog > "$_changelogfile"

	[ -f "$_changelogfile" ] && {
		_info "Generated repo changelog in '$_changelogfile'."
	}
}

environment_check

opt_arg_process "oiru'Uhgft'lmks" $@

[ -n "$no_option" ] && {
	clone_all
	prepare_linux
	setup_dl
	install_feeds $OWRT_CLONE_DIR
	_info "Clone preparation completed. Use './scripts/ltq_change_environment.sh switch' from ${OWRT_CLONE_DIR}/ to select a model and build."
}

[ -n "$f_option" ] && {
	O_FORCE=1;
}

[ -n "$i_option" ] && {
	clone_all
	prepare_linux
	setup_dl
	reinstall_feeds $OWRT_CLONE_DIR;
}

[ -n "$o_option" ] && {
	clean_all
}

[ -n "$r_option" ] && {
	reset_all
}

[ -n "$m_option" ] && {
	models_per_variant
}

[ -n "$h_option" ] && {
	_help 0;
}

[ -n "$u_option" ] && {
	update_clone;
}

[ -n "$U_option" ] && {
	update_clone 1;
}

[ -n "$l_option" ] && {
	generate_changelog
}

[ -n "$g_option" ] && {
	generate_tag_manifest;
}

[ -n "$t_option" ] && {
	generate_composite_tag "$t_opt_args" "$opt_args"
}

[ -n "$k_option" ] && {
	confirm_action "This action will clean linux repos, removes applied patches and resets the source code!"
	clean_linux_clone
	clean_linux
}

[ -n "$s_option" ] && {
	sync_linux_mirror;
}

[ -n "$opt_args" ] && {
	update_clone_manifest $opt_args;
	clone_all
	prepare_linux
	setup_dl
	install_feeds $OWRT_CLONE_DIR;
	_info "Clone preparation completed. Use './scripts/ltq_change_environment.sh switch' from ${OWRT_CLONE_DIR}/ to select a model and build."
}

popd >/dev/null
